<?php

namespace Myapp;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    //

     public $timestamps = false;

  	 protected $table = 'variants';

  	 public function sentence (){

  	     return $this->hasOne('Myapp\Sentence', 'id','id_sentence');

     }
    public function task (){

        return $this->belongsTo('Myapp\Task');
    }
}
