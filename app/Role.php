<?php

namespace Myapp;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = 'roles';

    protected $fillable = [
        'name'
    ];

    public function users () {
        return $this->belongsToMany('Myapp\User','role_user','id_role','id_user');
    }


}
