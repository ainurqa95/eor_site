<?php

namespace Myapp;

use Illuminate\Database\Eloquent\Model;

class UsersTasks extends Model
{
    //
      protected $table = 'usersTasks';

    protected $fillable = ['id_theme', 'score','id_users'];

    public function theme (){

        return $this->hasOne('Myapp\Theme', 'id','id_theme');

    }

}
