<?php

namespace Myapp;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $table = 'tasks';




    public function variant (){

        return $this->hasMany('Myapp\Variant', 'id','id_var');

    }
    public function sentence (){

        return $this->hasMany('Myapp\Sentence', 'id','id_sentence');

    }
    public function theme (){

        return $this->hasMany('Myapp\Theme', 'id','id_theme');

    }
}
