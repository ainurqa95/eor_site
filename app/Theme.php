<?php

namespace Myapp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Theme extends Model
{
    //
    use SoftDeletes;
    public $timestamps = false;

    protected $table = 'themes';

    protected $dates = ['deleted_at'];

    public function task (){

        return $this->belongsTo('Myapp\Task');
    }
    public function usersTask (){

        return $this->belongsTo('Myapp\UsersTasks');
    }

}
