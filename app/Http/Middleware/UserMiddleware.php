<?php

namespace Myapp\Http\Middleware;

use Closure;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if($request->route('id' ) != '1'){
//            return redirect()->route('home');
        echo "Мы вошли в посредник";  // это выведится уже до ответа
//        }
        $response =   $next($request);
//        return $next($request);
         echo "Мы вышли из посредник"; // это выведится уже после того как вернем ответ
        return $response;
    }
}
