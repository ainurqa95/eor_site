<?php

namespace Myapp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (!Auth::check()){
            return redirect('/login');
        }
        dump($user);
        return view('admin.index');

    }
    public function showThemes()
    {
        $user = Auth::user();
        if (!Auth::check()){
            return redirect('/login');
        }
        dump($user);
        return view('admin.index');

    }
}
