<?php

namespace Myapp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Myapp\UsersTasks;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $userId = Auth::user()->id; // нашли id авторизованно пользователя
        $usersResults = UsersTasks::where('id_users',$userId)->with('theme')->get(); // нашли пройденный результат с его темой
        $sum = UsersTasks::where('id_users',$userId)->sum('score'); // нашли сумма по score

        $data = array(
            'usersResults' => $usersResults,
            'sum' => $sum
        );
        return view('home',$data);
    }
}
