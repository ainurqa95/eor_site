<?php

namespace Myapp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\Store as SessionStore;
use DB;
use Illuminate\Support\Facades\Auth;
use Myapp\Theme;
use Myapp\UsersTasks;

//
/**
* 
*/
class SiteController extends Controller
{
    private $sizeOfWords = 7;

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

	public function index ()
	{
		return view('site.welcome');

	}
	public function courses()
	{

		// $themes = DB::select('select * from themes');
		$themes = Theme::all();
		$data = array ('title' => "Курсы", 'courses' => $themes);

		DB::listen( function( $query){
//			dump($query->sql);

		});

		if (view()->exists('site.courses'))
			return view('site.courses', $data);
		abort(404);

	}

	public function startCourse (Request $request, $id){
        if (view()->exists('site.start-course')){
            $theme = Theme::where('id',$id)->first();
            $data = array('id' => $id, 'theme' => $theme->name, 'statement' => $theme->statement );

            return response()->view('site.start-course', $data);
        }
        abort(404);
    }
	public function taskGo (Request $request, $id){ // для перовго отображения страницы

        // находим варианты ответов, для каждого предложения, одного таска, определенной темы
		$firstTask = DB::table('tasks')->join('sentences', function ($join) use($id) {
            $join->on('tasks.id_sentence', '=', 'sentences.id')
                 ->where('tasks.id_theme', '=', $id);
        })->join('variants','variants.id','=','tasks.id_var')->join('themes','themes.id','=','tasks.id_theme')->get(); //inRandomOrder()->get();

//        $task = Task::with('variant')->with('theme')->where('id_theme','=',1)->with('sentence')->get();

        $firstTask = $firstTask[0]; // нашли задачу определенного номера
		$variants = json_decode($firstTask->value_json ,True);
		$data = array( 'tasks'=> $variants[0] ,
            'title' => $firstTask->name,
            'sentence' => $firstTask->value,
            'rightAnswer' => $firstTask->answer,
            'id' => $id,
            'statement' => $firstTask->statement,
            'negation' => $firstTask->negation,
            'question' => $firstTask->question,

            );

		if (view()->exists('site.task')){
			return response()->view('site.task', $data);
		}

		
	}
    public function taskNext (Request $request){ // при нажатии на кнопку выбора слова

	    $numberWord =  $request->input('numberWord'); // текущий номер слова
        $numberTask = $request->input('numberTask'); // номер задачи
        $id_theme = $request->input('id_theme'); // id темы
        $firstTask = DB::table('tasks')->join('sentences', function ($join) use($id_theme) {
            $join->on('tasks.id_sentence', '=', 'sentences.id')
                ->where('tasks.id_theme', '=', $id_theme);
        })->join('variants','variants.id','=','tasks.id_var')->join('themes','themes.id','=','tasks.id_theme')->get();
        $firstTask = $firstTask[$numberTask]; // нашли задачу определенного номера
        $numberWord++; // переходим к следущему слову
        $variants = json_decode($firstTask->value_json ,True);
        if (!empty($variants[$numberWord])) { // если есть еще слова в предложении
            $data = array('tasks' => $variants[$numberWord], 'title' => $firstTask->name, 'sentence' => $firstTask->value, 'numberTask' => $numberTask, 'numberWord' => $numberWord, 'rightAnswer' => $firstTask->answer, 'complete' => false);
        }
        else { // если достигли лимита слов в предложении
//            $numberTask++;
//            $numberWord = 0;
            $numberWord = 0; // обнулили номер слова
            if ($numberTask < $this->sizeOfWords){ // если не достигли последнего таска
               // остановился здесь

                $numberTask++;
                $firstTask = DB::table('tasks')->join('sentences', function ($join) use($id_theme) {
                    $join->on('tasks.id_sentence', '=', 'sentences.id')
                        ->where('tasks.id_theme', '=', $id_theme);
                })->join('variants','variants.id','=','tasks.id_var')->join('themes','themes.id','=','tasks.id_theme')->get();
                $firstTask = $firstTask[$numberTask]; //
                $variants = json_decode($firstTask->value_json ,True);
                $data = array('tasks'=> $variants[$numberWord] , 'title' => $firstTask->name,'sentence' => $firstTask->value,'numberTask' => $numberTask,'numberWord' => $numberWord, 'rightAnswer' => $firstTask->answer, 'complete' => true );

            }else{ // если мы в конце игры
                // не делаем $numberTask++;
                $firstTask = DB::table('tasks')->join('sentences', function ($join) use($id_theme) { // если вдруг мы нажали на еще раз должны отправить предыдущий таск
                    $join->on('tasks.id_sentence', '=', 'sentences.id')
                        ->where('tasks.id_theme', '=', $id_theme);
                })->join('variants','variants.id','=','tasks.id_var')->join('themes','themes.id','=','tasks.id_theme')->get();
                $firstTask = $firstTask[$numberTask]; //
                $variants = json_decode($firstTask->value_json ,True);
                // добавили флаг game_over
                $data = array('game_over' => true,'tasks'=> $variants[$numberWord] , 'title' => $firstTask->name,'sentence' => $firstTask->value,'numberTask' => $numberTask,'numberWord' => $numberWord, 'rightAnswer' => $firstTask->answer, 'complete' => true );

            }
        }
        echo  json_encode($data); //json_encode($request->all());

    }
    public function addScore (Request $request){

        $score = $request->input('score'); // номер задачи

        $idTheme = $request->input('idTheme'); // id темы
        $idUser = Auth::user()->id;

        UsersTasks::create([
            'score' => $score,
            'id_theme' => $idTheme,
            'id_users' => $idUser
        ]);



    }

	public function about($id)
	{
		echo "id = " . $id;
        $firstTask = DB::table('tasks')->join('sentences', function ($join) use($id) {
            $join->on('tasks.id_sentence', '=', 'sentences.id')
                ->where('tasks.id_theme', '=', $id);
        })->join('variants','variants.id','=','tasks.id_var')->join('themes','themes.id','=','tasks.id_theme')->first();
		 // return view('site\page');
	}
		public function getPosts()
	{
		
		 // return view('site\page');
	}
		public function getOnePost($id)
	{
		echo "id = " . $id;
		 // return view('site\page');
	}
}