<?php

namespace Myapp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Myapp\Http\Requests\SignUpRequest;
use Myapp\Role;
use Myapp\Task;
use Myapp\Theme;
use Myapp\User;
use Myapp\Variant;

class UserController extends Controller
{
    //
    public function signIn(Request $request)
    {

        $data = array();
        print_r($request->all());

        if ($request->isMethod('post')){

            $email = $request->input('email','default');
            echo "email = " . $email;
            print_r($request->path());
            $request->flash(); // сохранили данные в сессию
            // $request->flashOnly('email' , 'password'); // записывает только email в сесс
            return redirect()->route('courses')->withInput();
        }
        // $result = $request->session()->all();//получаем данные из сессии
        //    $token = $result['_token'];

        if (view()->exists('user.sign-in')){
            // return view('site.sign-in', $data);
            $view = view('user.sign-in', $data);
            // return (new Response($view));
            return response()->view('user.sign-in', $data);
            // return response()->json(['name' => 'Hello' , 'user' => 'arty']); // формирует json
            // return response()->download('robots.txt'); // скачаивание файла находится в папке
        }



        abort(404);

    }
    public function signUp(Request $request)
    {

//        $task = Task::with('variant')->with('theme')->where('id_theme','=',1)->with('sentence')->get();
//
//        $user = User::find(1);
//        $role = new Role( ['name'=> 'quest' ]); // добавление
//        $user->roles()->save($role);
//        dump($task);
        $data = array();
//        print_r($request->all());


        if ($request->isMethod('post')){
            $rules = [
                'name' => 'required|max:60',
                'email' => 'required|email',
                'password' => 'required|min:5|max:60',
                'password_confirm' => 'required|min:5|max:60'
            ];
            $password = $request->input('password');
            $password_confirm = $request->input('password_confirm');
            echo "passwC = " . $password_confirm;
            $this->validate($request,$rules); // 1 способ
            $messages = []; //2 способ
            $validator = Validator::make( $request->all(), ['name' => 'required'],$messages);
            if ($validator->fails() ){
                if ($password_confirm != $password)
                $validator->errors()->add('password_confirm','Доп сообщение');
//                dump($validator->errors());
                redirect()->route('sign-up')->withErrors($validator)->withInput();
            }

//            User::create([
//                'name' => $input->name,
//            ]);

        }

        if (view()->exists('user.sign-up')){

            return response()->view('user.sign-up', $data);

        }

        abort(404);

    }
}
