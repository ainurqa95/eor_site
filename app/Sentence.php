<?php

namespace Myapp;

use Illuminate\Database\Eloquent\Model;

class Sentence extends Model
{
    //
        public $timestamps = false;

  		 protected $table = 'sentences';

  		 public function variant (){

  		     return $this->belongsTo('Myapp\Variant');
         }
        public function task (){

            return $this->belongsTo('Myapp\Task');
        }
}
