var advcake_int = {
    $advcake_chanel_get: 'utm_medium', //cpa,cpc,cpo ..,
    $advcake_campaign_get: 'utm_source', //admitad,cityads,actionpay ..,
    $advcake_webmaster_get: 'utm_campaign', //23441,NA4A,43111 ..,
    $advcake_domain: '.lingualeo.com',
    $advcake_send_ur: 'lingualeo',
    init: function () {
        this.sendTrackid();
        this.sendSB();
        this.checkOrd();
        this.sendHistory();
    },
    setCookie: function (e, t, n) {
        n = n || {};
        var o = n.expires;
        if ("number" == typeof o && o) {
            var r = new Date;
            r.setTime(r.getTime() + 1e3 * o), o = n.expires = r
        }
        o && o.toUTCString && (n.expires = o.toUTCString()), t = encodeURIComponent(t);
        var i = e + "=" + t;
        for (var a in n) {
            i += "; " + a;
            var c = n[a];
            c !== !0 && (i += "=" + c)
        }
        document.cookie = i
    },
    uniqid: function (pr, en) {
        var pr = pr || '',
            en = en || false,
            result;

        this.seed = function (s, w) {
            s = parseInt(s, 10).toString(16);
            return w < s.length ? s.slice(s.length - w) : (w > s.length) ? new Array(1 + (w - s.length)).join('0') + s : s;
        };

        result = pr + this.seed(parseInt(new Date().getTime() / 1000, 10), 8) + this.seed(Math.floor(Math.random() * 0x75bcd15) + 1, 5);

        if (en) result += (Math.random() * 10).toFixed(8).toString();

        return result;
    },
    IDGenerator: function (aaaa) {
        this.length = aaaa, this.timestamp = +new Date;
        var e = function (e, t) {
            return Math.floor(Math.random() * (t - e + 1)) + e
        };
        this.generate = function () {
            for (var t = this.timestamp.toString(), n = t.split("").reverse(), o = "", r = 0; r < this.length; ++r) {
                var i = e(0, n.length - 1);
                o += n[i]
            }
            return o
        }
    },
    getCookie: function (e) {
        var t = document.cookie.match(new RegExp("(?:^|; )" + e.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"));
        return t ? decodeURIComponent(t[1]) : ''
    },
    $_GET: function (e) {
        var t = window.location.search;
        return t = t.match(new RegExp(e + "=([^&=]+)")), t ? t[1] : ''
    },
    user_unic_ac_id: function () {
        if (this.getCookie('user_unic_ac_id') == '') {
            var generator = new this.IDGenerator(15);
            var user_unic_ac_id = generator.generate();
            this.setCookie("user_unic_ac_id", user_unic_ac_id, {
                expires: 60 * 60 * 24 * 31,
                domain: this.$advcake_domain,
                path: "/"
            });
        } else {
            var user_unic_ac_id = this.getCookie('user_unic_ac_id');
        }
        return user_unic_ac_id;
    },
    sendTrackid: function () {
        if (this.$_GET(this.$advcake_campaign_get) != '') {
            var trackid = this.uniqid();
            this.setCookie("advcake_trackid", trackid, {
                expires: 2678400,
                domain: this.$advcake_domain,
                path: "/"
            });
            var data_partner = this.$_GET("advcake_params");
            var iframe_param = (window === window.top) ? 0 : 1;
            var width_advcake = 0;
            var height_advcake = 0;
            if (typeof document !== 'undefined') {
                if (typeof document.documentElement !== 'undefined' && typeof document.documentElement.clientWidth !== 'undefined' && typeof document.documentElement.clientHeight !== 'undefined' && typeof document.documentElement.clientWidth.toString !== 'undefined' && typeof document.documentElement.clientHeight.toString !== 'undefined') {
                    width_advcake = document.documentElement.clientWidth.toString();
                    height_advcake = document.documentElement.clientHeight.toString();
                }
            }
            var webmaster_id_ac = this.$_GET(this.$advcake_webmaster_get);
            var type_ac = this.$_GET(this.$advcake_chanel_get);
            var campaign_ac = this.$_GET(this.$advcake_campaign_get);
            src = '//antifraud.adv-cake.ru/' + this.$advcake_send_ur + '/track_api.php' + '?user_main_id=' + this.user_unic_ac_id() + '&webmaster=' + webmaster_id_ac + '&type=' + type_ac + '&partner=' + campaign_ac + '&trackid=' + trackid + '&data_partner=' + data_partner + '&iframe=' + iframe_param + '&w=' + width_advcake + '&h=' + height_advcake;
            (new Image()).src = src;
        }
    },
    sendSB: function () {
        var page_id;
        if (this.getCookie('advcake_sb') == '' || this.$_GET(this.$advcake_campaign_get) != '') {
            var generator = new this.IDGenerator(32);
            page_id = generator.generate();
            this.setCookie("advcake_sb", page_id, {
                expires: 60 * 30,
                domain: this.$advcake_domain,
                path: "/"
            });

        } else {
            page_id = this.getCookie('advcake_sb');
            this.setCookie("advcake_sb", page_id, {
                expires: 60 * 30,
                domain: this.$advcake_domain,
                path: "/"
            });
        }
        if (this.$_GET(this.$advcake_campaign_get) != '') {
            var webmaster_id_ac = this.$_GET(this.$advcake_webmaster_get);
            var type_ac = this.$_GET(this.$advcake_chanel_get);
            var campaign_ac = this.$_GET(this.$advcake_campaign_get);
            var src = '//antifraud.adv-cake.ru/' + this.$advcake_send_ur + '/sb.php?pid=' + page_id + '&uid=' + this.user_unic_ac_id() + '&t=payed&wid=' + webmaster_id_ac + '&mtype=' + type_ac + '&cid=' + campaign_ac;
        } else {
            var src = '//antifraud.adv-cake.ru/' + this.$advcake_send_ur + '/sb.php?pid=' + page_id + '&uid=' + this.user_unic_ac_id() + '&t=direct';
        }
        (new Image()).src = src;
    },
    checkOrd: function () {
        if (typeof window.advcake_order_price != 'undefined' && window.advcake_order_price != '' && typeof window.advcake_order_id != 'undefined' && window.advcake_order_id != '') {
            this.send_ord(window.advcake_order_id, window.advcake_order_price);
        } else {
            if (typeof window.advcake_order_id != 'undefined' && window.advcake_order_id != '') {
                this.send_ord(window.advcake_order_id, 0);
            }
        }
        window.advcake_order = function (ord_id, ord_price) {
            window.advcake_order_id = ord_id;
            window.advcake_order_price = ord_price;
            this.send_ord(ord_id, ord_price);
        };
    },
    send_ord: function (o_id, o_price) {
        var ord_id = o_id;
        var ord_price = o_price;
        var trackid = this.getCookie('advcake_trackid');
        var src = '//antifraud.adv-cake.ru/' + this.$advcake_send_ur + '/ord.php?i=' + ord_id + '&p=' + ord_price + '&t=' + trackid;
        (new Image()).src = src;
    },
    sendHistory: function () {
        var referrer = document.referrer;
        var hostname = location.href;
        var webmaster_id_ac, campaign_ac;
        if (referrer == '') {
            referrer = '(direct)';
        }

        this.setCookie('advcake_session', '1', {
            expires: 3600,
            domain: this.$advcake_domain,
            path: "/"
        });
        if (this.$_GET(this.$advcake_webmaster_get) != '') {
            this.setCookie('advcake_utm_content', this.$_GET(this.$advcake_webmaster_get), {
                expires: 2678400,
                domain: this.$advcake_domain,
                path: "/"
            });
            webmaster_id_ac = this.$_GET(this.$advcake_webmaster_get);
        } else {
            if (this.getCookie('advcake_utm_content') != '') {
                webmaster_id_ac = this.getCookie("advcake_utm_content");
            } else {
                webmaster_id_ac = '';
            }
        }
        if (this.$_GET(this.$advcake_campaign_get) != '') {
            this.setCookie('advcake_utm_campaign', this.$_GET(this.$advcake_campaign_get), {
                expires: 2678400,
                domain: this.$advcake_domain,
                path: "/"
            });
            campaign_ac = this.$_GET(this.$advcake_campaign_get);
        } else {
            if (this.getCookie('advcake_utm_campaign') != '') {
                campaign_ac = this.getCookie("advcake_utm_campaign");
            } else {
                campaign_ac = '';
            }
        }
        (new Image()).src = '//antifraud.adv-cake.ru/' + this.$advcake_send_ur + '/track_user_history.php' + '?id=' + this.user_unic_ac_id() + '&u_web=' + webmaster_id_ac + '&u_par=' + campaign_ac + '&ref=' + encodeURIComponent(referrer) + '&land=' + encodeURIComponent(hostname);
    }
};


(function(){
    advcake_int.init();
})();