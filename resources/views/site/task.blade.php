<html lang="ru" xml:lang="ru" "=""><!--<![endif]-->
<head>
    <script type="text/javascript" id="veConnect" async="" crossorigin="anonymous"
            src="https://config1.veinteractive.com/scripts/5.0/capture-apps-5.0.0.js"></script>
    <script type="text/javascript" async="" id="topmailru-code" src="https://top-fwz1.mail.ru/js/code.js"></script>
    <script src="https://connect.facebook.net/signals/config/454321181439104?v=2.7.21" async=""></script>
    <script async="" src="//connect.facebook.net/en_US/fbevents.js"></script>
    <style type="text/css" id="styles-cache">@font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;

        }

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;

        }

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            src: local('Open Sans Semi Bold'), local('OpenSans-SemiBold'), url(data:application/x-font-woff;charset=utf-8;base64, format('woff');
        }
    </style>
    <!-- Google Analytics Content Experiment code -->
    <script type="text/javascript" async="" src="https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js"></script>
    <script type="text/javascript" async="" src="https://d31qbv1cthcecs.cloudfront.net/atrk.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script async="" src="//www.googletagmanager.com/gtm.js?id=GTM-T5DF9X"></script>
    <script>function utmx_section() {
        }

        function utmx() {
        }

        (function () {
            var
                k = '84048599-16', d = document, l = d.location, c = d.cookie;
            if (l.search.indexOf('utm_expid=' + k) > 0) return;

            function f(n) {
                if (c) {
                    var i = c.indexOf(n + '=');
                    if (i > -1) {
                        var j = c.indexOf(';', i);
                        return escape(c.substring(i + n.length + 1, j < 0 ? c.length : j))
                    }
                }
            }

            var x = f('__utmx'), xx = f('__utmxx'), h = l.hash;
            d.write(
                '<sc' + 'ript src="' + 'http' + (l.protocol == 'https:' ? 's://ssl' :
                '://www') + '.google-analytics.com/ga_exp.js?' + 'utmxkey=' + k +
                '&utmx=' + (x ? x : '') + '&utmxx=' + (xx ? xx : '') + '&utmxtime=' + new Date().valueOf() + (h ? '&utmxhash=' + escape(h.substr(1)) : '') +
                '" type="text/javascript" charset="utf-8"><\/sc' + 'ript>')
        })();
    </script>
    <script src="https://ssl.google-analytics.com/ga_exp.js?utmxkey=84048599-16&amp;utmx=&amp;utmxx=&amp;utmxtime=1506341673033"
            type="text/javascript" charset="utf-8"></script>
    <script>utmx('url', 'A/B');</script>
    <!-- End of Google Analytics Content Experiment code -->
    <script src="/build/static-maps/1708.1.3-rc1-ru-css.js"></script>
    <script src="/build/static-maps/1708.1.3-rc1-images.js"></script>
    <script src="/build/static-maps/1708.1.3-rc1-js.js"></script>
    <script src="/build/static-maps/1708.1.3-rc1-ru-i18n.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>(function () {
            var queue = [],
                isReady = false;

            window.leoPerformance = function (callback) {
                isReady ? callback.call(leoPerformance) : queue.push(callback);
            };

            leoPerformance.domLoading = Date.now();
            document.onreadystatechange = function () {
                var readyState = document.readyState;
                if (readyState === 'interactive') {
                    leoPerformance.domInteractive = Date.now();
                } else if (readyState === 'complete') {
                    leoPerformance.domComplete = Date.now();
                    isReady = true;
                    queue.forEach(function (callback) {
                        leoPerformance(callback);
                    });
                }
            };
        })();</script>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">

    <meta name="keywords"
          content="английский язык онлайн, английский онлайн, английский, язык, онлайн, фильмы, аудиокниги, уроки, рассказы, изучение, статьи, лексика, смотреть онлайн, чтение, диалоги, песни, самоучитель, аудио, видео, интерактивный, учить, методика, программа, бесплатно, обучение, уроки, курсы, школа, произношение, разговорный, тест, носители языка">
    <meta name="language" content="ru">
    <meta name="robots" content="index, follow">
    <meta name="alexaverifyid" content="AQdgvLi1T0iQnZE1kgELRuCgIBo">
    <meta name="yandex-verification" content="5d35279041ffc1c0">

    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="177123425663371">
    <meta property="og:site_name" content="Lingualeo">
    <meta property="og:title" content="Покори английский с Lingualeo">
    <meta property="og:description"
          content="Самый крутой сервис для изучения английского языка! Быстро, эффективно и интересно!">
    <meta property="og:image"
          content="http://staticcdn.lingualeo.com/1e89341a2/images/sharing/banner-for-material_4.png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:locale" content="ru_RU">
    <meta property="twitter:site" content="@LingualeoRus">
    <link rel="alternate" hreflang="pt" href="https://lingualeo.com/pt/grammar/rules">
    <link rel="alternate" hreflang="uk" href="https://lingualeo.com/uk/grammar/rules">
    <link rel="alternate" hreflang="tr" href="https://lingualeo.com/tr/grammar/rules">
    <link rel="alternate" hreflang="es" href="https://lingualeo.com/es/grammar/rules">
    <link rel="alternate" hreflang="es_LA" href="https://lingualeo.com/es_LA/grammar/rules">

    <meta name="google-site-verification" content="RK3DXCHVTEpfAROro8X7MdeJXgxYHbYU7ehzsFfExbc">
    <meta name="title" content="Грамматика">
    <meta name="description"
          content="Английский язык онлайн бесплатно. Аудио и видеоматериалы на любой вкус - фильмы, диалоги, тексты, клипы. Онлайн самоучитель английского языка. Изучение английского онлайн в игровой форме, по фильмам и песням с субтитрами. Упражнения для тренировки и запоминания английских слов, фраз.">
    <title>Грамматика</title>
    <!-- <meta name="apple-itunes-app" content="app-id=480952151"> -->
    <meta name="google-play-app" content="app-id=com.lingualeo.android">
    <link rel="image_src" href="/new-leo-logo.png">
    <link rel="shortcut icon" href="/favicon.ico">

    <link rel="apple-touch-icon-precomposed" sizes="60х60"
          href="//staticcdn.lingualeo.com/f00240352/images/apple-touch/icon-60x60-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76"
          href="//staticcdn.lingualeo.com/d0474b3d2/images/apple-touch/icon-76x76-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120"
          href="//staticcdn.lingualeo.com/e3f188062/images/apple-touch/icon-120x120-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152"
          href="//staticcdn.lingualeo.com/ed72c7532/images/apple-touch/icon-152x152-precomposed.png">
    <link rel="apple-touch-icon" href="//staticcdn.lingualeo.com/0e3ce56c2/images/apple-touch/icon.png">

    <link rel="search" type="application/opensearchdescription+xml" title="Lingualeo" href="/opensearch.xml">
    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/nglbhlefjhcjockellmeclkcijildjhi">
    <link rel="stylesheet" type="text/css" media="screen"
          href="//staticcdn.lingualeo.com/build/css/ru/main-combine.60c648.css">

    <link rel="stylesheet" type="text/css" media="screen"
          href="//staticcdn.lingualeo.com/build/css/ru/styles-combine.1312c5.css">


    <link rel="stylesheet" type="text/css" media="screen"
          href="//staticcdn.lingualeo.com/build/css/ru/quest/quest-main.1d9623.css">


    <link rel="stylesheet" type="text/css" media="screen"
          href="//staticcdn.lingualeo.com/build/css/ru/grammar/grammar-page.0f380b.css">

    <link rel="stylesheet" type="text/css" media="screen"
          href="//staticcdn.lingualeo.com/build/css/ru/grammar/grammar-main.e68f14.css">

    <link rel="stylesheet" type="text/css" media="screen"
          href="//staticcdn.lingualeo.com/build/css/ru/trainings/training-grammar.05c3b5.css">

    <script> var CONFIG_GLOBAL = {
            "jsBuilderHost": "js.sandbox.lingualeo-funk.com",
            "userCard": {
                "id": 16908407,
                "name": "Айнур Ахметгалиев",
                "ava_s": "https:\/\/contentcdn.lingualeo.com\/uploads\/avatar\/0s25.png",
                "ava_m": "https:\/\/contentcdn.lingualeo.com\/uploads\/avatar\/0s60.png",
                "ava_l": "https:\/\/contentcdn.lingualeo.com\/uploads\/avatar\/0s100.png",
                "place": null,
                "xp_level": 1,
                "lang_level": "Средний",
                "gold": false,
                "premiumType": "free",
                "sex": 1,
                "is_online": 0,
                "dailyExperience": 0,
                "registration_time": "1505296150"
            },
            "isProgramMode": false,
            "abTests": ["grammar", "ugcSentencesButton"],
            "registerDate": "2017-09-13",
            "userId": 16908407,
            "uiPreferences": {"fb-share-dialog-showed": null},
            "fiveDaysQuest": {
                "currentDay": {
                    "giftName": "meat",
                    "bonusSize": 10,
                    "day": 0,
                    "isDone": false,
                    "rewardType": "meatball",
                    "bonus": ""
                },
                "dailyRewards": [{"type": "meat", "count": 10}, {"type": "xp", "count": 25}, {
                    "type": "meat",
                    "count": 20
                }, {"type": "xp", "count": 50}, {
                    "type": "super",
                    "rewards": [{"type": "gold", "period": "P1D"}, {"type": "goldDiscount", "period": "P1D"}]
                }],
                "questReward": "+10&nbsp;фрикаделек"
            },
            "isActivePremium": false,
            "premiumType": false,
            "hintSection": "",
            "hintHideAll": 0,
            "hiddenHints": [],
            "leoDialogAction": "",
            "dailyBonus": null,
            "birthday": {"isBirthday": false},
            "isActivated": 0,
            "meatballsEatenCountLimit": 100,
            "promocode": "9llfb5",
            "promocodeUrl": "https:\/\/lingualeo.com\/ru\/r\/9llfb5",
            "dailyHours": 0.5,
            "normaHourlyPoints": 90,
            "notifyCount": 1,
            "userEmail": "ainur_ahmetgalie@mail.ru",
            "timezone": 180,
            "googleConfirmURL": "https:\/\/accounts.google.com\/o\/oauth2\/auth?client_id=314065986637-mbhtfur8sb2k9b88eljb4medpo4qforo.apps.googleusercontent.com&redirect_uri=https:\/\/lingualeo.com\/oauth2callback\/google&scope=https:\/\/www.google.com\/m8\/feeds\/&response_type=code&state=import_contacts_json",
            "userLangLevel": 4,
            "dialogData": {
                "task_num": 0,
                "task_num_prev": 0,
                "task_state": 0,
                "task_actions_finished": [[3, 3]],
                "meatballs": 200,
                "leoClothing": 0
            },
            "dialogs": {},
            "xpLevel": {
                "current_xp_level": 1,
                "current_xp_level_points": 0,
                "next_xp_level_points": 25,
                "xp_points_on_current_level": 21
            },
            "quests": {
                "showQuestDialog": false,
                "actionHintsEnabled": false,
                "advancedQuestSwitcher": true,
                "version": "f"
            },
            "userServerTime": 1506352472,
            "intros": {"training.savannah": false, "training.audition_plus": false},
            "features": {"uauth": 1, "cdnSpeedTestEnabled": 0},
            "utcServerTime": 1506341672,
            "isAuth": true,
            "assetHost": "\/\/staticcdn.lingualeo.com\/",
            "staticDomain": "staticcdn.lingualeo.com",
            "environment": "prod",
            "deviceName": "Linux",
            "i18n": {
                "target": "en",
                "local": "ru",
                "iface": "ru",
                "options": {
                    "blogLink": "http:\/\/corp.lingualeo.com\/ru\/",
                    "careersLink": "http:\/\/corp.lingualeo.com\/ru\/careers\/",
                    "dictionaries": {"multitran": true, "yandex": {"url": "http:\/\/translate.yandex.ru\/"}},
                    "invitePartners": {"vkontakte": true, "yandex": true, "mailru": true, "rambler": true},
                    "isShowOnChooseLanguageLanding": true,
                    "loginSocialNetworks": {
                        "vk": {"isMain": true, "isEnabled": true},
                        "ok": {"isMain": false, "isEnabled": true},
                        "facebook": {"isMain": false, "isEnabled": true},
                        "google": {"isMain": false, "isEnabled": true}
                    },
                    "reviews": true,
                    "support": true,
                    "supportedSocialNetworks": {
                        "vkontakte": {"url": "http:\/\/vk.com\/lingualeo"},
                        "facebook": {"url": "http:\/\/www.facebook.com\/Lingualeo"},
                        "twitter": {"url": "https:\/\/twitter.com\/LinguaLeoRus"}
                    },
                    "termsOfUseLink": "http:\/\/corp.lingualeo.com\/ru\/termsofuse\/",
                    "aboutLink": "http:\/\/corp.lingualeo.com\/ru\/about-lingualeo\/",
                    "androidStoreLink": "https:\/\/play.google.com\/store\/apps\/details?id=com.lingualeo.android",
                    "appStoreLink": "http:\/\/stor.re\/UA9d\/ztcX",
                    "authorsPublicOfferLink": "http:\/\/corp.lingualeo.com\/ru\/public-offer-for-courses-authors\/",
                    "partnership": "\/ru\/corporate\/",
                    "defaultLandingName": "flat-landing",
                    "googleAnalyticsId": "UA-13253747-1",
                    "isOptimizelyTestEnabled": true,
                    "optimizelyAllowedInModules": [{"module": "dashboard"}, {"module": "dashboard2"}, {
                        "module": "default",
                        "action": "index"
                    }, {"module": "survey"}],
                    "isPromoTabEnabled": true,
                    "optimizelyExperimentCode": 1040380111,
                    "supportEmail": "support@lingualeo.com",
                    "winphoneStoreLink": "https:\/\/www.microsoft.com\/ru-ru\/store\/p\/%D0%90%D0%BD%D0%B3%D0%BB%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9-%D1%81-lingualeo\/9wzdncrfj09c",
                    "teachersHelpLink": "https:\/\/help.lingualeo.com\/hc\/ru\/categories\/201200713-%D0%A3%D1%87%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F-%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%8F",
                    "cacheDir": "\/var\/www\/1708.1.3-rc1\/src\/LinguaLeo\/ConfigBundle\/..\/..\/..\/cache",
                    "baseDir": "\/var\/www\/1708.1.3-rc1\/src\/LinguaLeo\/ConfigBundle\/..\/..\/..\/"
                },
                "localeList": {
                    "pt": "Português (Brasil)",
                    "ru": "Русский",
                    "tr": "Türkçe",
                    "es": "Español",
                    "es_LA": "Español (América Latina)"
                },
                "region": "ru",
                "betaLanguages": ["uk"]
            },
            "prefixUrl": "\/ru",
            "social": {
                "vkId": 3042279,
                "fbId": 177123425663371,
                "fbScope": "email,public_profile,user_friends,user_birthday"
            },
            "payment": {"currency": "руб.", "currencyCode": "RUB", "currencyId": 0},
            "voice": "ukenglishmale",
            "page": "",
            "leoAvatarIndex": 1,
            "events": {"guard": null},
            "forceHtmlAudioPlayer": false,
            "translateMenu": {"globalListening": false},
            "dataLayer": [{
                "event": "userInfo",
                "userInfo": {
                    "user_id": 16908407,
                    "user_email": "ainur_ahmetgalie@mail.ru",
                    "user_lang_local": "ru",
                    "user_lang_target": "en",
                    "user_langlevel": 4,
                    "user_premium_type": "free",
                    "user_xp_level": 1,
                    "user_currency": "RUB",
                    "abtest": "none",
                    "vk_id": "26498070"
                }
            }],
            "version": "1708.1.3-rc1"
        }; </script>
    <script src="//staticcdn.lingualeo.com/build/js/common/style-storage.e243d9.min.js"></script>

    <script src="//staticcdn.lingualeo.com/build/js/common/di-core.504117.min.js"></script>


    <script src="//staticcdn.lingualeo.com/build/js/config.i18n.ru.2cd04e.min.js"></script>
    <script src="//staticcdn.lingualeo.com/build/js/config.af9ffb.min.js"></script>

    <script>leoPerformance.layoutStart = Date.now();</script>
    <style type="text/css">.first-d-popup {
            text-align: center;
            padding: 40px 60px;
            max-width: 400px
        }

        .first-d-popup__img {
            width: 180px;
            height: 127px;
            margin: auto
        }

        .first-d-popup__title {
            font-size: 32px;
            margin: 30px 0 20px
        }

        .first-d-popup__text {
            margin-bottom: 20px;
            line-height: 25px
        }

        .first-d-popup__text-bold {
            border-radius: 3px;
            background-color: #FBD50B;
            font-weight: 700;
            padding: 1px 4px 2px
        }

        .first-d-popup__price {
            font-size: 32px;
            margin: 20px 0
        }

        .first-d-popup__price-old {
            color: rgba(0, 0, 0, .2);
            display: inline-block;
            text-decoration: line-through
        }

        .first-d-popup__price-new {
            display: inline-block;
            color: #00B670;
            margin-left: 15px
        }

        .first-d-popup__button {
            font-size: 16px;
            padding: 16px 20px 15px
        }
        .progress-steps__i.progress-steps__i_state_WRONG_RED > i{
            background-color: darkred;

        }

    </style>
    <script src="https://track.adform.net/Serving/TrackPoint/?pm=778165&amp;ADFPageName=ROS&amp;ADFdivider=%7C&amp;ord=129697010546&amp;Set1=ru%7Cru%7C1366x768%7C24&amp;ADFtpmode=2&amp;loc=https%3A%2F%2Flingualeo.com%2Fru%2Fgrammar%2Frules%3Ffrom%3Dtop-menu"
            async=""></script>
</head>
<body class="background-default   authorized-user background-green lang-ru native-lang-ru chrome chrome60.0.3112.113 linux show-notification-panel full-screen-page">
<div data-notification-panel="" class="notification-panel extension-panel">
    <div class="notification-content" data-notification-content=""><i class="iconbr-chrome"></i> Переводите слова и
        фразы со всего интернета и тренируйте их на Lingualeo! <a class="btn-blue-flat btn-size-small"
                                                                  href="/ru/browserapps" data-browser-link="">Подробнее
            о расширении</a></div>
    <a class="close-icon icon-close-big" data-notification-panel-close=""></a></div>

<!-- Google Tag Manager -->
<noscript>&lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-T5DF9X"
    height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;
</noscript>
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push(
            {'gtm.start': new Date().getTime(), event: 'gtm.js'}
        );
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T5DF9X');</script>
<!-- End Google Tag Manager -->

{{--CSRF--}}

<meta name="csrf-token" content="{{ csrf_token() }}">


<div data-wrapper-content="" class="l-wrapper-content l-center  grammar-rules trangram_show_train">
    <div class="b-header l-center" data-page-header="">
        <div class="b-header__inner">
            <div class="b-header__right">

                <a href="/ru/glossary/learn/dictionary" class="b-dict-link b-header__dict" data-a-target="topmenu-dict">
                    Словарь </a>

                <div class="b-header__user b-info-user" data-a-target="topmenu-account">
                    <a class="b-info-user__link " href="/ru/profile"
                       style="background-image: url(//contentcdn.lingualeo.com/uploads/avatar/0s50.png)"></a>

                    <div class="b-userpopup ">
                        <div class="b-userpopup__t">
                            <a href="/ru/profile" class="b-userpopup__ava
                                "
                               style="background-image: url(//contentcdn.lingualeo.com/uploads/avatar/0s100.png)"></a>
                            <div class="b-userpopup__content">
                                <a href="/ru/profile" class="b-userpopup__title t-ellps">
                                    Айнур Ахметгалиев
                                </a>
                                <div class="b-userpopup__email t-ellps">
                                    ainur_ahmetgalie@mail.ru
                                </div>
                                <div class="b-userpopup__info">
                                    <a href="/ru/meatballs" class="b-userpopup__info-i type_meat"
                                       data-tooltip="На страницу фрикаделек" data-tooltip-offsety="5"
                                       data-tooltip-pos="bottom">
                                        200
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="b-userpopup__b">
                            <a href="/ru/premium?from=status" class="b-userpopup__link type_gold"
                               data-a-target="topmenu-link-gold">
                                Premium </a>
                            <a href="/ru/referral" class="b-userpopup__link">
                                Пригласи друзей </a>
                            <a href="/ru/premium-gift?from=profile-menu" class="b-userpopup__link type_gift">
                                Подарок другу </a>

                            <a href="/ru/social" class="b-userpopup__link" data-a-target="topmenu-link-social">
                                Друзья </a>
                            <a href="http://help.lingualeo.com/hc/ru" class="b-userpopup__link"
                               data-ga-click="Faq,dropdown_menu_click" data-a-target="topmenu-link-faq" target="_blank">
                                Ответы на вопросы </a>


                            <a class="b-userpopup__link" href="/ru/profile/edit" data-a-target="topmenu-link-profile">
                                Настройки </a>
                            <a data-logout-link="" class="b-userpopup__link" href="/logout"
                               data-a-target="topmenu-link-logout">
                                Выход </a>
                        </div>
                    </div>
                </div>


                <div class="b-header__notif">
                                <span class="b-header__notif b-header-notif show-loading notified"
                                      data-quest-help-container="block-e-0-3" data-notify-tab=""
                                      data-a-target="topmenu-notify">
        <a href="javascript: void 0" class="b-header-notif__link">
            <span data-notify-count="" class="count">1</span>
        </a>
        <div class="b-header__notif-popup notify-list hidden">
            <div data-notify-list="" class="notify-list-inner"></div>
               <div class="notify-load-text hidden">
                   <i class="ico-loading"></i>
               </div>
        </div>
    </span>

                </div>


                <div class="b-header__sale" data-header-default-sale-container="">
                    <div class="l-sale-link" data-element="top-gold-link-container">
                        <a data-a-target="topmenu-gold" data-promise-sale-topbar-doodle=""
                           class="icons-premium type-sept1 l-sale-link__inner" data-element="top-gold-link"
                           href="/ru/dashboard">
                            в англию<br>с Лео </a>
                    </div>
                </div>
            </div>

            <div class="b-header__left">
                <a class="b-logo b-header__logo" href="/ru" title="Lingualeo — английский язык онлайн">
                    <i class="logo-img"></i> </a>

                <a class="b-header__link-back" href="/">
                    Перейти на сайт </a>

                <div class="b-header__unreg-text">
                    так английский ты еще не учил!
                </div>

                <div class="b-header__unreg-buttons">
                    <a href="/ru/register" class="b-header__unreg-btn btn-white-l" data-jungle-auth=""
                       data-auth-type="registration">Регистрация</a>
                    <a href="/ru/login" class="b-header__unreg-btn btn-white-l" data-jungle-auth=""
                       data-auth-type="login">Вход</a>
                </div>

                <ul class="b-menu b-menu_type_main">
                    <li class=" ">
                        <a href="/ru/dashboard" class="main-link main-link-dashboard ">
                            Задания
                        </a>
                    </li>
                    <li class=" ">
                        <a href="/ru/jungle" class="main-link main-link-jungle ">
                            Материалы
                        </a>
                    </li>
                    <li class=" " data-quest-help-container="block-e-0-2 block-e-0-9 block-d-1-8 block-d-0-14">
                        <a href="/ru/training" class="main-link main-link-training ">
                            Тренировки
                        </a>
                    </li>
                    <li class="  selected ">
                        <a href="/ru/grammar/rules?from=top-menu" class="main-link main-link-grammar ">
                            Грамматика
                        </a>
                    </li>
                    <li class=" " data-menutab-course="" data-quest-help-container="block-e-0-10">
                        <a href="/ru/course" class="main-link main-link-course ">
                            Курсы
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="l-content l-center ">
        <div class="l-content-inner">
            <script> CONFIG.pages.grammar = {"disabledRulePages": false}; </script>

            <div data-grammar="">
                <div data-reactroot="" class="grammar-content">
                    <div class="traingram-page training-page_state_Mech" style="height: 600px;">
                        <div class="traingram-head">
                            <div class="traingram-head__close-con"><a class="traingram-head__close"
                                                                      href="/courses"></a></div>
                            <div class="traingram-head__title">{{ isset($title) ? $title : ""}}</div>
                        </div>
                        <div class="traingram-page__slider">
                            <div class="traingram-slider">
                                <div class="traingram-slider__anim">
                                    <div class="traingram-slider__slide">
                                                    <div class="b-results-container hidden">
                                                        <div class="b-final b-final-grammar">
                                                            <div class="b-final__slider">
                                                                <div class="b-final__slider-inner">
                                                                    <div class="b-final__slider-i b-final__slider-i-main current">
                                                                        <div class="b-final__slider-content">
                                                                            <div class="b-final__slider-title">Неплохо, но есть над
                                                                                чем поработать!
                                                                            </div>
                                                                            <div class="b-final__slider-subtitle">6 из 10
                                                                                предложений было собрано без ошибок
                                                                            </div>
                                                                        </div>
                                                                        <div class="b-slide-content">
                                                                            <div class="b-progress__add"><span><div
                                                                                            class=" b-progress__add-icon"><i
                                                                                                class="icons-final-time"></i></div><span>Повторим правило<br>через 3 дня</span></span>
                                                                            </div>
                                                                            <div class="b-dailynorm">
                                                                                <div class="b-progresult">
                                                                                    <div class="b-progresult__inner">
                                                                                        <div class="b-progresult__track"
                                                                                             style="height: 0%;">
                                                                                            <div class="b-progresult__plus"
                                                                                                 style="height: 14.4px;">
                                                                                                <div class="b-progresult__plus-bg"></div>
                                                                                            </div>
                                                                                            <div class="b-progresult__track-bg"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="b-progresult__text">
                                                                                        <div class="b-progresult__slide">
                                                                                            <div class="b-progresult__i type-first">
                                                                                                <div class="b-progresult__text-percents">
                                                                                                    <span class="number"><span id="capacity" data-digits-capacity="1">8</span>
                                                                                                        <!-- react-text: 2035 -->%
                                                                                                        <!-- /react-text --></span><span>Твой результат<br>{{$title}}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="b-progresult__i type-second">
                                                                                                <div class="b-progresult__text-xp">
                                                                                                    <span class="number"><!-- react-text: 2040 -->6
                                                                                                        <!-- /react-text --><span
                                                                                                                class="hint"><!-- react-text: 2042 -->/
                                                                                                            <!-- /react-text -->
                                                                                                            <!-- react-text: 2043 -->75
                                                                                                            <!-- /react-text --></span></span>
                                                                                                    <!-- react-text: 2044 -->очков
                                                                                                    опыта<!-- /react-text --></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="b-progresult__anim-white"></div>
                                                                                    <div class="b-progresult__anim-green"></div>
                                                                                    <div class="b-progresult__anim-lap"></div>
                                                                                </div>
                                                                                <div class="b-progress__add">
                                                                                    <div class="b-progress__add-old"><span><div class="b-progress__add-icon">
                                                                                                <i class="icons-final-xp"></i></div>
                                                                                            Опыт +6
                                                                                            </span></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="b-final__footer"><a class="b-final__btn" href="/courses">Выбрать следующее
                                                                    задание</a>
                                                                <div><a class="b-final__btn-sub" href="/ru/grammar/rules">К списку
                                                                        правил</a><!-- react-text: 2058 -->&nbsp;
                                                                    <!-- /react-text --><a href="/courses/{{$id}}" class="b-final__btn-sub">Продолжить
                                                                        тренировку</a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        <div class="card-screen">
                                            <div class="card-screen__inner">

                                                <div class="card-screen__translation">
                                                    <div class="card-screen__translation-inner">{{ $sentence }}
                                                    </div>
                                                </div>


                                                <div class="card-screen__placeholder-con">
                                                    <div id="card-screen__placeholder" class="card-screen__placeholder">
                                                        <div class="card-screen__placeholder-text"></div>
                                                    </div>
                                                    {{--<div class="card-screen__popup">--}}
                                                    {{--<div data-card-popup="" class="card-screen__popup-answer">--}}
                                                    {{--you dating anyone--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}


                                                    <span class="card-screen__cursor"></span><a
                                                            class="card-screen__delete" data-id="0"  ></a><i
                                                            class="card-screen__sound"></i>
                                                </div>


                                                <div class="card-screen__card-con">

                                                    @foreach ($tasks as $k => $task)

                                                        @foreach($task as $word)
                                                            <div class="card-variant"><span
                                                                        class="card-variant__text card-variant__text-anim-visible capitalize big-size">{{$word}}</span><span
                                                                        class="card-variant__text card-variant__text-anim-hidden"></span>
                                                            </div>


                                                        @endforeach

                                                    @endforeach

                                                </div>


                                                <a class="btn-bordered-l card-screen__info-btn">Показать правило</a>
                                            </div>
                                            <div class="card-screen__result hidden "><a
                                                        class="btn-blue-light-l card-screen__again-btn" data-id="0">Еще
                                                    раз</a>
                                                <div><a class="btn-bordered-l card-screen__result-btn">Показать
                                                        правило</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="traingram-slider__slide"></div>
                                </div>
                            </div>
                        </div>
                        <div class="traingram-footer">
                            <div class="traingram-footer__progress progress-steps">
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS">
                                    <i class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                                <div class="progress-steps__i progress-steps__i_state_PROGRESS"><i
                                            class="progress-steps__i-circle"></i></div>
                            </div>
                        </div>
                        <div class="gram-info gram-info_type_train hidden">
                            <div class="gram-info__header">
                                <div class="gram-info__header-title">{{ $title }}</div>
                                <div class="gram-info__header-tab">
                                    <div class="gram-info__tab gram-info__tab_type_positive gram-info__tab_type_selected">
                                        утверждение
                                    </div>
                                    <div class="gram-info__tab gram-info__tab_type_question">вопрос</div>
                                    <div class="gram-info__tab gram-info__tab_type_negation">отрицание</div>
                                </div>
                            </div>
                            <div class="gram-info__inner" id="statement">
                               {!! $statement !!}
                            </div>
                            <div class="gram-info__inner" style="display: none" id="question">
                                {!! $question !!}
                            </div>
                            <div class="gram-info__inner" style="display: none" id="negation">
                                {!! $negation !!}
                            </div>
                            <div class="gram-info__close-con"><a class="gram-info__close">Закрыть</a></div>
                            <div class="gram-info__loading"><i class="ico-loading"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="globalFade" class="global-fade-main hidden"></div>
        </div>
    </div>


</div>
<div id="fadeBlack" class="global-fade-black hidden"></div>
<div class="l-wrapper-footer  ">
    <div class="l-board" data-page-board="">
        <div class="l-board-inner l-center clearfix">

            <div class="user-progress l-board__progress" id="xpControls">
                <div class="user-progress__track">
                    <div class="user-progress__count" data-xp-points-left="">ещё 4 очка</div>
                    <div class="user-progress__value" data-xp-progressbar="" style="width: 84%;"></div>

                    <div class="user-progress__level">
                        <span data-level-number="">1</span>
                    </div>
                </div>

                <div class="user-progress__text">уровень</div>
            </div>


            <a href="/ru/journal?from=satiety_indicator">
                <div id="hungerControls" class="user-hungry l-board__hungry" data-tooltip-pos="top left"
                     data-quest-help-container="block-d-0-9"
                     data-tooltip-style-class="tooltip_type_bright user-hungry__tooltip fixed"
                     data-tooltip="Сегодня вы ещё не занимались">

                    <div class="user-hungry__bar" data-hunger-progressbar="" style="height: 10%;"></div>
                    <div class="user-hungry__text" data-hunger-progresstitle="">0</div>
                </div>
            </a>


            <div id="leonAvatarContainer" data-body-click-hider-element="0">
                <div class="leo-avatar l-board__avatar" data-leo-avatar="">
                    <div class="leo-avatar__big leo-style-0" data-leo-inner=""></div>
                    <div class="leo-avatar__min leo-style-0" data-leo-inner=""></div>
                </div>

                <div data-quest-bubble="" class="quest-progress-current quest-progress-new">
                    <div class="quest-bl__current hidden" data-quest-current-block="">
                        <div data-quest-holder="" class="quest-bl__big qcurrent-lg">
                            <div class="quest-bl__holder "><i class="qcurrent-lg__icon"></i>
                                <h2 class="qcurrent-lg__title"> Привет, Я Лео! </h2>
                                <div class="qcurrent-lg__subtext"> Я помогу тебе выучить английский.<br>Когда встречаешь
                                    незнакомые слова — кликай по ним, чтобы узнать перевод. А если кликнуть по переводу
                                    — слово добавится в твой словарь. <br><br>Смотри, я нашел для тебя ролик. Учимся
                                    переводить незнакомые слова.
                                </div>
                                <div class="qcurrent-lg__tasks qaction_type_lg">
                                    <div class="qaction-item   qaction-item_type_no-number ">
                                        <div class="t-ellps"><span class="qaction-item__num">1. </span> Кликай по
                                            словам, смотри перевод
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript: void 0" class="btn-blue-flat qcurrent-lg__btn"
                                   data-quest-button="start"> Приступить! </a>
                                <div class="quest-award qcurrent-lg__awards">
                                    <div class="quest-award__offset">
                                        <div class="quest-award__item">
                                            <div data-tooltip-style-class="tooltip_type_bright quest-award__item-tooltip"
                                                 data-tooltip="Награда:<br>10 очков опыта" class="quest-award__xp"> +10
                                                <i class="quest-award__xp-icon"></i></div>
                                        </div>
                                        <div class="quest-award__item"><i
                                                    data-tooltip-style-class="tooltip_type_bright quest-award__item-tooltip"
                                                    data-tooltip="Награда:<br>Jeans для Лео"
                                                    class="quest-award__img award-thing award-thing-1"></i></div>
                                    </div>
                                </div>
                                <a href="javascript: void 0" class="qcurrent-lg__close" data-close-quest=""
                                   data-tooltip="Закрыть" data-tooltip-pos="right top"> </a>
                                <div class="quest-bl__loading hidden"><i class="ico-loading"></i></div>
                            </div>
                        </div>
                        <div class="quest-bl__min qcurrent-sm" data-bubble-min="">
                            <div class="qnew-sm__text qaction_type_sm">
                                <div class="qaction-item   qaction-item_type_no-number ">
                                    <div class="t-ellps"><span class="qaction-item__num">1. </span> Кликай по словам,
                                        смотри перевод
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="quest-bl__min qnew-sm " data-quest-button="start">
                            <div class="qnew-sm__text"> Давай познакомлю с сервисом?</div>
                        </div>
                    </div>
                    <div class="quest-bl__complete hidden" data-quest-complete-block="">
                        <div data-quest-holder="" class="quest-bl__big qcomplete-lg">
                            <div class="quest-bl__holder"><i class="icons-check-big qcomplete-lg__icon"></i>
                                <h2 class="qcomplete-lg__title">Задание выполнено!</h2>
                                <div class="qcomplete-lg__awards"><h4 class="qcomplete-lg__award-title"> Ты получил
                                        награду </h4>
                                    <div class="qcomplete-lg__award-list quest-award quest-award_type_white">
                                        <div class="quest-award__offset">
                                            <div class="quest-award__item">
                                                <div data-tooltip-style-class="tooltip_type_bright quest-award__item-tooltip"
                                                     data-tooltip="Награда:<br>10 очков опыта" class="quest-award__xp">
                                                    +10 <i class="quest-award__xp-icon"></i></div>
                                            </div>
                                            <div class="quest-award__item"><i
                                                        data-tooltip-style-class="tooltip_type_bright quest-award__item-tooltip"
                                                        data-tooltip="Награда:<br>Jeans для Лео"
                                                        class="quest-award__img award-thing award-thing-1"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript: void 0" class="btn-blue-flat qcomplete-lg__btn" data-show-new=""> К
                                    новому заданию </a> <a href="javascript: void 0" class="qcomplete-lg__close"
                                                           data-close-quest="" data-tooltip="Закрыть"
                                                           data-tooltip-pos="right top"> </a>
                                <div class="quest-bl__loading qcomplete-lg__loading hidden"><i class="ico-loading"></i>
                                </div>
                            </div>
                        </div>
                        <div class="quest-bl__min qcomplete-sm" data-bubble-min=""><i class="qcomplete-sm__icon"></i>
                            <div class="qcomplete-sm__text t-ellps"> Задание выполнено!</div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="xpBubble" class="xp-bubble xp-bubble_type_default l-board__bubble">
                <span class="xp-bubble__text">+25</span>
            </div>
        </div>
    </div>
    <div class="l-footer l-center ">
        <div class="clearfix"></div>

        <div class="l-footer-bottom  state_light" data-page-element="footer">
            <div class="l-browser-links" data-browser-extension-link=""><a
                        href="https://chrome.google.com/webstore/detail/lingualeo/nglbhlefjhcjockellmeclkcijildjhi?utm_source=lingualeo&amp;utm_medium=referral&amp;utm_campaign=from_browserapps"
                        target="_blank" class="" data-ga-click="Extensions,footer_click_chrome"
                        data-tooltip="Расширение для Chrome" data-tooltip-pos="top right"><i
                            class="l-icon-footer l-icon-footer-chrome"></i></a></div>
            <div class="l-mobile-sep"></div>

            <div class="l-mobile-links">
                <a href="/ru/mobile" data-ga-click="Footer,mobile_links" class="link-corp-1 highlighted">Мобильные
                    приложения</a>
            </div>


            <a href="http://help.lingualeo.com/hc/ru" target="_blank" class="l-menu-link menu-link-faq"
               data-ga-click="Footer,click_faq">FAQ</a>


            <a href="/ru/feedback" class="l-menu-link menu-link-feedback" data-ga-click="Footer,click_feedback">Поддержка</a>

            <a href="http://corp.lingualeo.com/ru/careers/" class="l-menu-link menu-link-job"
               data-ga-click="Footer,click_jobs" target="_blank">Вакансии</a>

            <a href="http://corp.lingualeo.com/ru/" class="l-menu-link menu-link-blog" target="_blank">Блог</a>

            <a href="/ru/corporate/" class="l-menu-link menu-link-corporate">Сотрудничество</a>

            <div class="l-social-links">
                <a href="http://vk.com/lingualeo" target="_blank" data-ga-click="Social,fotter_click_vk">
                    <i class="l-icon-footer l-icon-footer-vk"></i>
                </a>
                <a href="http://www.facebook.com/Lingualeo" target="_blank"
                   data-ga-click="Social,footer_click_facebook">
                    <i class="l-icon-footer l-icon-footer-facebook"></i>
                </a>
                <a href="https://twitter.com/LinguaLeoRus" target="_blank" data-ga-click="Social,footer_click_twitter">
                    <i class="l-icon-footer l-icon-footer-twitter"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<noscript>&lt;div class="browser-panel"&gt;В вашем браузере отключен JavaScript.&lt;br/&gt; Пожалуйста, включите его в
    настройках, чтобы пользоваться сервисом Lingualeo.&lt;/div&gt;
</noscript>
<div id="fb-root"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var numberWord = 0; // номер слова текущий
        var numberTask = 0; // номер предложения, для которого используем
        var url = document.URL;
        var id_theme = url.match(/[0-9]/ig); // id темы
        var sentence = "";
        var rightAnswer = "";

        $(document).on("click", ".btn-bordered-l.card-screen__info-btn", function () {

            $('.traingram-page__slider').hide();
            $('.gram-info__content').css('background-color','white');
            $('.traingram-head').hide();
            $('.gram-info.gram-info_type_train.hidden').attr('class','gram-info gram-info_type_train');


        });
        $(document).on("click", ".gram-info__close", function () {

            $('.traingram-page__slider').show();
            $('.gram-info__content').css('background-color','');
            $('.traingram-head').show();
            $('.gram-info.gram-info_type_train').attr('class','gram-info gram-info_type_train hidden');


        });
        $(document).on("click", ".gram-info__tab.gram-info__tab_type_question", function () {
                $('#statement').hide();
                $('#negation').hide();
                $('#question').show();

                $('.gram-info__tab.gram-info__tab_type_negation.gram-info__tab_type_selected').attr('class','gram-info__tab gram-info__tab_type_negation');
                $('.gram-info__tab.gram-info__tab_type_positive.gram-info__tab_type_selected').attr('class','gram-info__tab gram-info__tab_type_positive');

                $(this).attr('class','gram-info__tab gram-info__tab_type_question gram-info__tab_type_selected');


        });
        $(document).on("click", ".gram-info__tab.gram-info__tab_type_negation", function () {
            $('#statement').hide();
            $('#question').hide();
            $('#negation').show();

            $('.gram-info__tab.gram-info__tab_type_question.gram-info__tab_type_selected').attr('class','gram-info__tab gram-info__tab_type_question');
            $('.gram-info__tab.gram-info__tab_type_positive.gram-info__tab_type_selected').attr('class','gram-info__tab gram-info__tab_type_positive');

            $(this).attr('class','gram-info__tab gram-info__tab_type_negation gram-info__tab_type_selected');



        });
        $(document).on("click", ".gram-info__tab.gram-info__tab_type_positive", function () {

            $('#question').hide();
            $('#negation').hide();
            $('#statement').show();
            $('.gram-info__tab.gram-info__tab_type_question.gram-info__tab_type_selected').attr('class','gram-info__tab gram-info__tab_type_question');
            $('.gram-info__tab.gram-info__tab_type_negation.gram-info__tab_type_selected').attr('class','gram-info__tab gram-info__tab_type_negation');
            $(this).attr('class','gram-info__tab gram-info__tab_type_positive gram-info__tab_type_selected');


        });




        $(document).on("click", ".card-variant", function () { // при нажатии на любое слово

            var answer = $(this).find('span').html(); // получаем слово на которое нажали
            $.ajax({
                type: 'POST',
                url: '/taskNext',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {

                    numberTask: numberTask,
                    numberWord: numberWord,
                    id_theme: id_theme


                },
                success: function (result) {

                    var width = answer.length * 25; // ширина
                    // добавляем ответы пользователя над блоком с словами
                    var ans = '<div class="card-screen__placeholder" style="font-size: 40px; width:  ' + width + 'px;"><div class="card-screen__placeholder-text card-screen__placeholder-text-moved"> ' + String(answer) + ' </div> </div>';
                    $('.card-screen__placeholder-con').append(ans); // добавляем ответы пользователя над блоком с словами
                    $('.card-variant').remove(); // удаляем все блоки
                    var data = JSON.parse(result); // получили выбор слов, номер задачи, номер предложения

                    var tasks = data['tasks']; // спсок слов
                    numberTask = data['numberTask']; // номер задачи
                    numberWord = data['numberWord']; // номер слова
                    var complete = data['complete']; //

                    console.log('numberTask =', numberTask);
                    console.log('numberWord =', numberWord);
                    console.log('complete =', complete);

                    $('.card-screen__delete').attr('data-id', numberWord); // добавили крестику номер слова

                    for (var key in tasks) {
                        var word = tasks[key];
                        var html = "";
                        for (var key2 in word) { // выводим варианты ответов слов
                            html += '<div class="card-variant"><span class="card-variant__text card-variant__text-anim-visible capitalize big-size">' + word[key2] + '</span><span class="card-variant__text card-variant__text-anim-hidden"></span> </div>';
                        }
                    }

                    if (complete) { // если нужно переходить к следущему предлжению
                        var answerSentence = '';
                        $.each($('.card-screen__placeholder-text.card-screen__placeholder-text-moved'), function (index, tab) {
                            answerSentence += $(this).html() + ' '; // получили все ответы пользователя

                        });
                        answerSentence = answerSentence.replace(/\s{2,}/g, ' '); // убираем не нужные пробелы
                        rightAnswer = rightAnswer.replace(/\s{2,}/g, ' ');
                        answerSentence = $.trim(answerSentence); // ответ пользователя
                        rightAnswer = $.trim(rightAnswer); // правильный ответ
                        console.log('sentence =', sentence);
                        console.log('answer sentence =', answerSentence);
                        console.log('right answer =', rightAnswer);



                        if (rightAnswer.localeCompare(answerSentence) == 0) { // если польователь правильно ответил на слова

                            $.each($('.card-screen__placeholder'), function (index, tab) { // удаляем слова выбранные пользователем
                                $(this).remove();

                            });

                            if (data['game_over']) { // если конец игры
                                $(".b-results-container.hidden").attr("class", "b-results-container"); // открываем окно конца игры
                                var tekStatus = $(".traingram-footer__progress.progress-steps").find('div').eq(numberTask).attr('class'); // статус прохождения
                                console.log("tekStatus = ", tekStatus);
                                if (tekStatus == "progress-steps__i progress-steps__i_state_PROGRESS") {
                                    $(".traingram-footer__progress.progress-steps").find('div').eq(numberTask).attr('class', 'progress-steps__i progress-steps__i_state_RIGHT'); // так как мы правильно ответили помечаем зеленым
                                }
                                $('.card-screen__translation-inner').remove();

                                $(".b-progresult__plus-bg").css("background-color","#f0f0f0");
                                var countRight = 0;
                                var points = 0;
                                $.each($(".traingram-footer__progress.progress-steps").find('div'), function (index, tab) { // удаляем слова выбранные пользователем
                                    if ($(this).attr('class') == "progress-steps__i progress-steps__i_state_RIGHT"){
                                        points = points + 3;
                                        countRight++;

                                    }else if($(this).attr('class') == "progress-steps__i progress-steps__i_state_WRONG"){
                                        points = points + 1;
                                    }




                                });
                                // храним результаты пользователя в бд

                                $.ajax({
                                    type: 'POST',
                                    url: '/addScore',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: {
                                        score: points,
                                        idTheme: id_theme[0]
                                    },
                                    success: function (result) {
                                        console.log(result);

                                    } // end success 2 ajax
                                }); // end 2 ajax
                                $(".b-final__slider-subtitle").html( String(countRight) + " из 10 предложений было собрано без ошибок");
                                console.log("right == ", countRight);
                                var percent = parseFloat(points/30).toFixed(2)*100;
                                $('#capacity').html(percent); // добавили процент ответов
                                $('#capacity').attr( 'data-digits-capacity', countRight);
                                $('.b-progress__add-old').html("<span><div class=\"b-progress__add-icon\">\n" +
                                "                                    <i class=\"icons-final-xp\"></i></div>\n" +
                                "                                Опыт + "+points+ "</span>");// добавляем опыт

                                // меняем имя результата вверху
                                if (countRight>9)
                                     $('b-final__slider-title').html("Отлично !");
                                else
                                    $('b-final__slider-title').html("Неплохо, но есть над чем работать !");
                                $('.card-screen').remove();


                                return;
                            }



                            $('.card-screen__translation-inner').html(data['sentence']); // кладем предложение вверху экрнана которое нужно собрать

                            var tekStatus = $(".traingram-footer__progress.progress-steps").find('div').eq(numberTask - 1).attr('class');
                            console.log("tekStatus = ", tekStatus);
                            if (tekStatus == "progress-steps__i progress-steps__i_state_PROGRESS") {
                                $(".traingram-footer__progress.progress-steps").find('div').eq(numberTask - 1).attr('class', 'progress-steps__i progress-steps__i_state_RIGHT');  // так как мы правильно ответили помечаем зеленым
                            }


                        } else { // если неправильно ответил
                            $('.card-screen__card-con').hide(); // убрали выбор слов
                            $('.card-screen__delete').hide(); // убрали крестик
                            $('.btn-bordered-l.card-screen__info-btn').hide(); // убрали показать правило лишнее
                            $('.card-screen__result').show(); // показали кнопку "еще раз"

                            rightAnswer = rightAnswer.split(' '); // разбиваем правильный ответ на слова
                            answerSentence = answerSentence.split(' '); // разбиваем ответ польователя на слова
                            console.log('right ans', rightAnswer);
                            $.each($('.card-screen__placeholder'), function (index, tab) { // удаляем слова выбранные пользователем
                                $(this).remove();

                            });

                            // формируем ошибочные слова
                            var countWrongWords = 0;   // число ошибочных слов
                            for (i = 0; i < rightAnswer.length; i++) { // проходим по всем правильным словам
                                var width = answerSentence[i].length * 25;
                                if (rightAnswer[i] != answerSentence[i]) { // если слова не совпали то показываем где ошиблись
                                    var wrongAnsw = '<span data-grammar-mistake="1" class="card-screen__mistake incorrect">' +
                                        '<div class="card-screen__placeholder inherit bad" data-card-word-placeholder="2" style="font-size: 40px; width:' + width + 'px;">' + String(answerSentence[i]) + '</div>' +
                                        '<div class="card-screen__popup"><div data-card-popup="" class="card-screen__popup-answer"> ' + rightAnswer[i] + ' </div> </div></span>';
                                    $('.card-screen__placeholder-con').append(wrongAnsw);
                                    countWrongWords++;
///
                                } else { // если совпали то просто выводим

                                    var rightAnsw = '<div class="card-screen__placeholder inherit" data-card-word-placeholder="1" style="font-size: 40px; width:' + width + 'px;">' + String(answerSentence[i]) + '</div>';
                                    $('.card-screen__placeholder-con').append(rightAnsw);
                                }

                            }


                            if (!data['game_over']) { // если конец игры отлчие в номере
                                $(".btn-blue-light-l.card-screen__again-btn").attr("data-id", numberTask - 1);

                                if (countWrongWords == 1)

                                    $(".traingram-footer__progress.progress-steps").find('div').eq(numberTask - 1).attr('class', 'progress-steps__i progress-steps__i_state_WRONG');
                                else
                                    $(".traingram-footer__progress.progress-steps").find('div').eq(numberTask - 1).attr('class', 'progress-steps__i progress-steps__i_state_WRONG_RED');
                            }
                            else { // data-id для кнопки еще раз, помечаем желтой кнопкой кнопкой, задание поскольку мы не прошли задание
                                $(".btn-blue-light-l.card-screen__again-btn").attr("data-id", numberTask);
                                if (countWrongWords == 1)
                                    $(".traingram-footer__progress.progress-steps").find('div').eq(numberTask).attr('class', 'progress-steps__i progress-steps__i_state_WRONG');
                                else
                                    $(".traingram-footer__progress.progress-steps").find('div').eq(numberTask).attr('class', 'progress-steps__i progress-steps__i_state_WRONG_RED');

                            }
                                console.log("numberTask", numberTask);
                            console.log("они не равны");
                        }


                    } else { //


                        rightAnswer = data['rightAnswer']; // получем правильный ответ из бд на пред шаге
                        sentence = data['sentence'];
                    }


                    $('.card-screen__card-con').append(html); // добавили слова для выбора


                } // end success 2 ajax
            }); // end 2 ajax
        });


        $(document).on("click", ".btn-blue-light-l.card-screen__again-btn", function () { // при нажатии на кнопку еще раз
//            numberTask = 0;

            numberWord = -1;

            numberTask = $(this).attr('data-id'); // получаем номер таска предыдущего задания

            $('.card-screen__card-con').show(); //  показываем слова

            $('.card-screen__delete').show(); // показывае кнопку крестика

            $('.btn-bordered-l.card-screen__info-btn').show(); // показываем кнопку

            $('.card-screen__result').hide(); // убираем кнопку еще раз

            $.each($('.card-screen__placeholder'), function (index, tab) {
                $(this).remove(); // удалем выбранные пользователем слова

            });
            $.each($('.card-screen__mistake.incorrect'), function (index, tab) { // удаляем неправильно выбранные слова
                $(this).remove();

            });
            $.ajax({
                type: 'POST',
                url: '/taskNext',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {

                    numberTask: numberTask,
                    numberWord: numberWord,
                    id_theme: id_theme


                },
                success: function (result) {


                    var data = JSON.parse(result); // получили ранее добвленные товары
                    console.log(data);

                    $('.card-variant').remove();
                    var tasks = data['tasks'];
                    numberTask = data['numberTask'];
                    numberWord = data['numberWord'];
                    var complete = data['complete'];
                    $('.card-screen__delete').attr('data-id', numberWord);

                    for (var key in tasks) { // выводим слова  с предыдущего шага
                        var word = tasks[key];
                        var html = "";
                        for (var key2 in word) {
                            console.log("word = ", word[key2]);

                            html += '<div class="card-variant"><span class="card-variant__text card-variant__text-anim-visible capitalize big-size">' + word[key2] + '</span><span class="card-variant__text card-variant__text-anim-hidden"></span> </div>';
                        }

                    }
                    $('.card-screen__card-con').append(html);


                } // end success 2 ajax
            }); // end 2 ajax


        }); // end btn cl
        $(document).on("click", ".card-screen__delete", function () { // при нажатии на кнопку еще раз
//

            numberWord = $('.card-screen__delete').attr('data-id');

            $('.card-screen__placeholder').last().remove();
            numberWord = numberWord -2;
            console.log("number wprd = ", numberWord);


            $.ajax({
                type: 'POST',
                url: '/taskNext',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {

                    numberTask: numberTask,
                    numberWord: numberWord,
                    id_theme: id_theme


                },
                success: function (result) {


                    var data = JSON.parse(result); // получили ранее добвленные товары
                    console.log(data);

                    $('.card-variant').remove();
                    var tasks = data['tasks'];
                    numberTask = data['numberTask'];
                    numberWord = data['numberWord'];
                    var complete = data['complete'];
                    $('.card-screen__delete').attr('data-id', numberWord);

                    for (var key in tasks) { // выводим слова  с предыдущего шага
                        var word = tasks[key];
                        var html = "";
                        for (var key2 in word) {
                            console.log("word = ", word[key2]);

                            html += '<div class="card-variant"><span class="card-variant__text card-variant__text-anim-visible capitalize big-size">' + word[key2] + '</span><span class="card-variant__text card-variant__text-anim-hidden"></span> </div>';
                        }

                    }
                    $('.card-screen__card-con').append(html);


                } // end success 2 ajax
            }); // end 2 ajax

//            alert(numberTask);


        });
        $(document).on("click", ".b-final__btn", function () { // при нажатии на кнопку еще раз
//
            alert("helo");
            $(location).attr('href', '/courses');
        });
    });


</script>


<script>leoPerformance.layoutEnd = Date.now();</script>
<!--[if lte IE 8]>
<script src="//staticcdn.lingualeo.com/js/lib/ecma5-ie8.js"></script>

<![endif]-->
<!--[if lte IE 8]>
<link rel="stylesheet" type="text/css" media="screen" href="//staticcdn.lingualeo.com/build/css/ru/ie/ie.2d42de.css"/>

<![endif]-->
<!--[if IE 9]>
<script src="//staticcdn.lingualeo.com/js/lib/ie9.js"></script>

<![endif]-->
<!--[if IE 9]>
<link rel="stylesheet" type="text/css" media="screen" href="//staticcdn.lingualeo.com/build/css/ru/ie/ie9.097715.css"/>

<![endif]-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="//staticcdn.lingualeo.com/build/js/react.a320f3.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>

<!--[if lte IE 9]>
<script src="//staticcdn.lingualeo.com/js/lib/jquery.history.js"></script>

<![endif]-->
<!--[if lte IE 9]>
<script src="//staticcdn.lingualeo.com/js/lib/shivs/placeholder.js"></script>

<![endif]-->

<script src="//staticcdn.lingualeo.com/build/js/common/vendors-core.8a5677.min.js"></script>


<script src="//staticcdn.lingualeo.com/build/js/i18n/lang.i18n.ru.28a7fa.min.js"></script>
<script src="//staticcdn.lingualeo.com/build/js/i18n/lang.e9e2d5.min.js"></script>


<script src="//staticcdn.lingualeo.com/js/var/quests/game_f_ru.js"></script>

<script type="text/javascript">


    // Google Analytics
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
</script>

<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
    _atrk_opts = {atrk_acct: "3Zd/i1a4ZP00gm", domain: "lingualeo.com", dynamic: true};
    (function () {
        var as = document.createElement('script');
        as.type = 'text/javascript';
        as.async = true;
        as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js";
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(as, s);
    })();
</script>
<noscript>&lt;img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=3Zd/i1a4ZP00gm" style="display:none"
    height="1" width="1" alt="" /&gt;
</noscript>
<!-- End Alexa Certify Javascript -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter837359 = new Ya.Metrika({
                    id: 837359,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>&lt;div&gt;&lt;img src="//mc.yandex.ru/watch/837359" style="position:absolute; left:-9999px;" alt="" /&gt;&lt;/div&gt;</noscript>
<!-- /Yandex.Metrika counter -->


<!-- Google Analytics -->
<script type="text/javascript">
    ga('create', 'UA-13253747-1', {
        cookieDomain: 'lingualeo.com'
    });
    ga('require', 'displayfeatures');
    ga('set', 'dimension2', '2017-09-13');
    ga('set', 'dimension3', '4');
    ga('set', 'dimension4', 'no');
    ga('set', 'dimension5', 'none');
    ga('send', 'pageview');
</script>

<script type="text/javascript">
    di.require(['$debug'], function ($debug) {

        //category: '', //Категория события
        //action: '', //Название события
        //data: '', //any additional data in json-string

        //userLangLocal: '',
        //userLangTarget: '',
        //userPremiumType: '',
        //userLangLevel: '',
        //userXPLevel: '',
        //abtest: '',
        //cookieUserID: '',
        //lingualeouid: '',
        //uid: '', //unique identifier for user

        //userAgent: '', //user agent
        //tv: '', //версия трекера
        //tna: '', //название трекера
        //aid: '', //application id
        //platform: '', //platform name
        //lang: '', //язык браузера
        //res: '', //разрешение экрана
        //vp: '', //viewport size
        //ds: '', //web page width and height
        //eid: '', //event id
        //page: '', //
        //refr: '', //referrer URL
        //url: '', //page URL
        //tz: '', //user timezone
        //dtm: '',


        //vid: '', //index of number visits that this user_id has made to this domain

        var contextOptions = {
            uid: '16908407',
            upt: '',
            ull: 4,
            uxl: 1,

            ab: 'none',
            luid: '1506240123281442'
        };

        di.config('$tracker', {
            cola: {
                collectorUrl: '//cola.lingualeo.com/pixel',
                context: contextOptions
            }
        });
    });
</script>
<script type="text/javascript">
    di.require(['$tracker'], function ($tracker) {
        $tracker.pushPage(location.href);
    });
</script>
<script src="//staticcdn.lingualeo.com/build/js/common.i18n.ru.8ab4db.min.js"></script>
<script src="//staticcdn.lingualeo.com/build/js/common.7db844.min.js"></script>
<div class="l-left-banner"><a class="l-close" data-close-banner="" data-ga-click="Mobile,left_click_close">х</a> <a
            class="l-iphone" href="http://stor.re/UA9d/ztcX" target="_blank" data-oid-track="iphone,mobile:backmenu"
            data-oid-custom-action="Download app"></a> <a class="l-android"
                                                          href="https://play.google.com/store/apps/details?id=com.lingualeo.android"
                                                          target="_blank" data-oid-track="android,mobile:backmenu"
                                                          data-oid-custom-action="Download app"></a> <a
            class="l-winphone"
            href="https://www.microsoft.com/ru-ru/store/p/%D0%90%D0%BD%D0%B3%D0%BB%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9-%D1%81-lingualeo/9wzdncrfj09c"
            target="_blank" data-oid-track="winphone,mobile:backmenu" data-oid-custom-action="Download app"></a></div>


<script src="//staticcdn.lingualeo.com/build/js/grammar.i18n.ru.6155a1.min.js"></script>
<script src="//staticcdn.lingualeo.com/build/js/grammar.f535de.min.js"></script>


<script>
    LEO.state && LEO.state.refreshState && LEO.state.refreshState({
        xp_level_pct: 84,
        xp_points_remaining: 4,
        xp_points_daily: 0,
        xp_points_daily_norm: 75,
        xp_level: 1,
        meatballs: 200,
        hungry: 0
    });
</script>


<script type="text/javascript" id="">google_tag_manager["GTM-T5DF9X"].macro('gtm4') ? !function (b, e, f, g, a, c, d) {
        b.fbq || (a = b.fbq = function () {
            a.callMethod ? a.callMethod.apply(a, arguments) : a.queue.push(arguments)
        }, b._fbq || (b._fbq = a), a.push = a, a.loaded = !0, a.version = "2.0", a.queue = [], c = e.createElement(f), c.async = !0, c.src = g, d = e.getElementsByTagName(f)[0], d.parentNode.insertBefore(c, d))
    }(window, document, "script", "//connect.facebook.net/en_US/fbevents.js") : window.fbq = function () {
        console.log("utm-event: facebook", arguments)
    };
    fbq("init", "454321181439104");
    fbq("track", "PageView");</script>
<noscript>
    &lt;img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=454321181439104&amp;amp;ev=PageView&amp;amp;noscript=1"&gt;
</noscript>
<script type="text/javascript" id="advcakeAsync">(function (b) {
        var a = b.createElement("script");
        a.async = 1;
        a.src = "//server.adv-cake.ru/antifraud/a.js?r\x3d" + Math.random();
        b.body.appendChild(a)
    })(document);</script>
<script async="" src="//server.adv-cake.ru/antifraud/a.js?r=0.5591402904331249"></script>

<script type="text/javascript" id="">var _tmr = window._tmr || (window._tmr = []);
    _tmr.push({
        id: "2919010",
        type: "pageView",
        start: (new Date).getTime(),
        pid: google_tag_manager["GTM-T5DF9X"].macro('gtm5')
    });
    (function (b, d, a) {
        if (!b.getElementById(a)) {
            var c = b.createElement("script");
            c.type = "text/javascript";
            c.async = !0;
            c.id = a;
            c.src = ("https:" == b.location.protocol ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
            a = function () {
                var a = b.getElementsByTagName("script")[0];
                a.parentNode.insertBefore(c, a)
            };
            "[object Opera]" == d.opera ? b.addEventListener("DOMContentLoaded", a, !1) : a()
        }
    })(document, window, "topmailru-code");</script>
<noscript>&lt;div&gt;

    &lt;img src="//top-fwz1.mail.ru/counter?id=2919010;js=na" style="border:0;position:absolute;left:-9999px;" alt=""&gt;

    &lt;/div&gt;
</noscript>


<script type="text/javascript" id="">var _tmr = _tmr || [];
    _tmr.push({
        type: "itemView",
        productid: "productid",
        pagetype: "pagetype",
        list: "list",
        totalvalue: "totalvalue"
    });</script>

<script type="text/javascript" id="">(function () {
        var a = google_tag_manager["GTM-T5DF9X"].macro('gtm10');
        a && 0 < a.orderId && ($form = $('\x3cform name\x3d"orderInfo"\x3e'), $form.append($('\x3cinput type\x3d"hidden" name\x3d"orderId" value\x3d"' + a.orderId + '" /\x3e')), $form.append($('\x3cinput type\x3d"hidden" name\x3d"currency" value\x3d"RUB" /\x3e')), $form.append($('\x3cinput type\x3d"hidden" name\x3d"summ" value\x3d"' + a.summRub + '" /\x3e')), $(".thanks-bl").append($form));
        google_tag_manager["GTM-T5DF9X"].macro('gtm12') ? jQuery.getScript("//config1.veinteractive.com/tags/01814143/64F8/42AC/AF6A/EE2BA85D1F29/tag.js") :
            console.log("veinteractive all page")
    })();</script>
<script async="" src="//antifraud.adv-cake.ru/b.php?r=0.2457909142308612"></script>
<script src="//config1.veinteractive.com/scripts/shared/vendor.js" crossorigin="anonymous" async=""></script>
<iframe id="ve-storage-iframe" tabindex="-1"
        src="https://config1.veinteractive.com/scripts/shared/iframeStorage-5.0.0.html?iframeid=ve-storage-iframe&amp;journeyId=44789"
        style="display: none;"></iframe>
<script async="" src="//antifraud.adv-cake.ru/lingualeo/o.js?r=0.398256148570755"></script>
</body></html>