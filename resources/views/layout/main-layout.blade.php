<!DOCTYPE html>
<html>
<head>
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">--}}
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <title>{{ isset($title) ? $title : "Курсы" }}</title>

</head>
<body>


<div class="container">

@section('header')
   <!--  {{ print_r(Session::all() )}} -->
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills float-right">
            <li class="nav-item">
              <a class="nav-link active" href="{{ route('home') }}"> {{ isset($title) ? $title : "Главная" }} <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('courses') }}"> Грамматика</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Тренировки</a>
            </li>
             <li class="nav-item">
                <a class="nav-link" href="{{ route ('sign-in')}}">Войти</a>

            </li>
              <li class="nav-item">

                  <a class="nav-link" href="{{ route ('sign-up')}}">Регистрация</a>
              </li>
          </ul>
        </nav>
        <h3 class="text-muted">English for everyone</h3>
      </div>
@endsection
@yield('header') 
<!-- yield вызывает секцию навбар -->


@section('navbar')
      <div class="jumbotron">
        <h1 class="display-3">Самый популярный способ учить английский онлайн</h1>
        <p class="lead">Изучайте английский, занимаясь по 5 минут в день в игровой форме. </p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Начать сейчас!</a></p>
      </div>
@endsection
@yield('navbar') 





@yield('content') 




@section('footer')
      <footer class="footer">
        <p>© English for everyone 2017</p>
      </footer>

@endsection
@yield('footer') 

    </div>
</body>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>