@extends('layout.main-layout')

@section('navbar')

@endsection


@section('content')
@if ($errors->any() > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error )
                <li> {{$error}}</li>
            @endforeach

        </ul>



    </div>

@endif
    <form class="form-horizontal" action='{{ route('sign-up')}}' method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset>
            <div id="legend">
                <legend class="">Регистрация</legend>
            </div>
            <div class="control-group">
                <!-- Username -->
                <label class="control-label"  for="name">Имя</label>
                <div class="controls">
                    <input type="text" value="{{ old('name') }}" id="name" name="name" placeholder="" class="input-xlarge">
                    <p class="help-block">Username can contain any letters or numbers, without spaces</p>
                </div>
            </div>

            <div class="control-group">
                <!-- E-mail -->
                <label class="control-label" for="email">E-mail</label>
                <div class="controls">
                    <input type="text" id="email" name="email" value="{{ old('email') }}" placeholder="" class="input-xlarge">
                    <p class="help-block">Please provide your E-mail</p>
                </div>
            </div>

            <div class="control-group">
                <!-- Password-->
                <label class="control-label" value="{{ old('password') }}" for="password">Пароль</label>
                <div class="controls">
                    <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
                    <p class="help-block">Password should be at least 4 characters</p>
                </div>
            </div>

            <div class="control-group">
                <!-- Password -->
                <label class="control-label"  for="password_confirm">Пароль (повторить)</label>
                <div class="controls">
                    <input type="password" id="password_confirm" name="password_confirm" placeholder="" class="input-xlarge">
                    <p class="help-block">Please confirm password</p>
                </div>
            </div>

            <div class="control-group">
                <!-- Button -->
                <div class="controls">
                    <button class="btn btn-success">Зарегестрироваться</button>
                </div>
            </div>
        </fieldset>
    </form>

@endsection