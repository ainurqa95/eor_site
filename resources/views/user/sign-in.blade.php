@extends('layout.main-layout')

@section('navbar')
	
@endsection


@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error )
                    <li> {{$error}}</li>
                @endforeach

            </ul>



        </div>

    @endif

<form class="form-signin" method="post" action="{{ route('sign-in')}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
        <h2 class="form-signin-heading">Выполните вход</h2>
        <label for="inputEmail" class="sr-only">Email </label>
        <input type="email" value="{{ old('password') }}" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
        <label for="inputPassword"  class="sr-only">Password</label>
        <input type="password" value="{{ old('password') }}" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" >Sign in</button>
      </form>


@endsection