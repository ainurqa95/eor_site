@extends('layouts.app')
@section('navbar')

@endsection


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Пройденные задания</div>

                <div class="panel-body">
                    @if (session('status'))

                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Очки</th>
                                    <th>Тема</th>
                                    <th>Дата прохождения</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 1; $i <= count($usersResults); $i++)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{intval($usersResults[$i-1]->score)}}</td>
                                        <td>{{ $usersResults[$i-1]->theme->name }}</td>
                                        <td>{{date_format($usersResults[$i-1]->created_at, 'Y-m-d H:i')}}</td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                        </div>

                    Сумма очков: {{intval($sum)}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
