<?php

use Illuminate\Database\Seeder;
use Myapp\Theme;

class ThemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 
//        DB::table('themes')->insert([
//            'name' => "Present Simple",
//
//        ]);
//        Theme::create([
//        	'name' => "Past Simple",
//        	]);
//        Theme::create([
//            'name' => "Future Simple",
//        ]);

        Theme::create([
            'name' => "Present Continious",
        ]);
        Theme::create([
            'name' => "Past Continious",
        ]);
        Theme::create([
            'name' => "Present Perfect",
        ]);
        Theme::create([
            'name' => "Past Perfect",
        ]);
        Theme::create([
            'name' => "Future Perfect",
        ]);
    }
}
