<?php

use Illuminate\Database\Seeder;
use Myapp\Sentence;


class SentencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Sentence::create([
//        	'value' => "Вы нуждаетесь в помощи",
//            'answer' => 'You need help',
//        	'id_theme' => 1,
//
//        	],
//
//            );
//        Sentence::create([
//            'value' => "Это не значит ничего",
//            'answer' => "It doesn't mean anything",
//            'id_theme' => 1,
//
//            ]);

//                Sentence::create([
//                    'value' => "Где это произошло",
//                    'answer' => "Where did it happen",
//                    'id_theme' => 2,
//
//                ]);
//                Sentence::create([
//                    'value' => "Я предпочитаю наличные",
//                    'answer' => "I prefer cash",
//                    'id_theme' => 1,
//
//                ]);
//                Sentence::create([
//                    'value' => "Они не верят мне",
//                    'answer' => "They don't believe me",
//                    'id_theme' => 1,
//
//                ]);
                Sentence::create([
                    'value' => "Куда я иду",
                    'answer' => "Where do I go",
                    'id_theme' => 1,

                ]);
                Sentence::create([
                    'value' => "Я знаю что-то",
                    'answer' => "I know something",
                    'id_theme' => 1,

                ]);
                Sentence::create([
                    'value' => "Где он работает",
                    'answer' => "Where does he work",
                    'id_theme' => 1,

                ]);
                Sentence::create([
                    'value' => "Ты выглядишь прекрасно",
                    'answer' => "You look fine",
                    'id_theme' => 1,

                ]);
                Sentence::create([
                    'value' => "Мы не знаем вас",
                    'answer' => "We don't know you",
                    'id_theme' => 1,

                ]);
                Sentence::create([
                    'value' => "Он не говорит ничего",
                    'answer' => "He doesn't say anything",
                    'id_theme' => 1,

                ]);
                Sentence::create([
                    'value' => "Почему идет дождь",
                    'answer' => "Why does it rain",
                    'id_theme' => 1,

                ]);




    }
}
