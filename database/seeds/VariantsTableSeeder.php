<?php

use Illuminate\Database\Seeder;
use Myapp\Variant;

class VariantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
//        		$array = [
//					['вы' => [ 'Your' , 'Helpped', 'Helping', 'Helps', 'You', 'Yoors'] ],
//					['нуждаетесь' => ['needer' ,'needing','needed', 'needded', 'need','needer']],
//					['в помощи' => ['helpped' ,'helping','help', 'helped', 'helps','helper']],
//
//				];
//
//					  Variant::create([
//	        				'value_json' => json_encode($array),
//	        				'id_sentence' => 5,
//	        	]);
//
//				$array = [
//					['это' => [ "Didn't" , "Its", 'Him', 'It', 'He', 'His'] ],
//					['не' => ['His' ,"doesn't", "didn't", 'He', 'its', "don't"]],
//					['значит' => ['meaner' ,'meant','means', 'mean', 'meaning', 'meanest']],
//					['ничего' => ['something' ,'anything','means', 'mean', 'meaning', 'its']],
//
//
//				];
//				Variant::create([
//	        				'value_json' => json_encode($array),
//	        				'id_sentence' => 6,
//	        	]);
//        $array = [
//            ['Где' => [ "Why" , "Who", 'Where', 'When', 'What', 'Has'] ],
//            ['это' => ['do' ,"done", "going", 'happening', 'did', "does"]],
//            ['произо' => ['it' ,"it's",'happen', 'does', 'his', 'where']],
//            ['произошло' => ['happied' ,'happen','happened', 'where', 'happening', 'was']],
//
//
//        ];
//        Variant::create([
//            'value_json' => json_encode($array),
//            'id_sentence' => 7,
//        ]);
//        $array = [
//            ['Я' => [ "Me" , "I", 'Mine', 'You', 'Prefer', 'My'] ],
//            ['предпочитаю' => ['prefer' ,"prefers", "prefered", 'prefering', 'preferest', "pfererer"]],
//            ['наличные' => ['cashing' ,"casual",'card', 'cash', 'cashed', 'cashes']],
//
//        ];
//        Variant::create([
//            'value_json' => json_encode($array),
//            'id_sentence' => 8,
//        ]);
//        $array = [
//            ['Они' => [ "Them" , "They", 'We', "Don't", "Didn't", 'Their'] ],
//            ['не' => ["don't" ,"didn't", "believe", 'not', 'believe', "pfererer"]],
//            ['верят' => ['beleives' ,"beleive",'believed', 'believing', 'believe', 'beleft']],
//            ['мне' => ['not' ,"them",'I', 'mine', 'believe', 'me']],
//
//
//        ];
//        Variant::create([
//            'value_json' => json_encode($array),
//            'id_sentence' => 9,
//        ]);
            $array = [
                ['0' => [ "When" , "Where", 'Do', "What", "Why", 'Going'] ],
                ['1' => ["did" ,"done", "do", 'does', 'doing', "where"]],
                ['2' => ['why' ,"go",'I', 'do', 'me', 'mine']],
                ['3' => ['goes' ,"going",'went', 'go', 'gone', 'gos']],
            ];
            Variant::create([
                'value_json' => json_encode($array),
                'id_sentence' => 10,
            ]);
            $array = [
                ['0' => [ "Me" , "I", 'Knowed', "Knows", "My", 'Mine'] ],
                ['1' => ["knowing" ,"know", "knew", 'knows', 'knowed', "known"]],
                ['2' => ['know' ,"something",'some', 'somthin', 'me', 'anything']],

            ];
            Variant::create([
                'value_json' => json_encode($array),
                'id_sentence' => 11,
            ]);
            $array = [
                ['0' => [ "When" , "Where", 'Do', "What", "Why", 'Going'] ],
                ['1' => ["did" ,"done", "do", 'does', 'doing', "where"]],
                ['2' => ['he' ,"him",'she', 'his', 'her', 'it']],
                ['3' => ['works' ,"working",'work', 'worked', 'workeft', 'he']],
            ];
            Variant::create([
                'value_json' => json_encode($array),
                'id_sentence' => 12,
            ]);
            $array = [
                ['0' => [ "You" , "Fine", 'Your', "Me", "Yours", 'Fining'] ],
                ['1' => ["looks" ,"lookest", "looking", 'looks', 'look', "looked"]],
                ['2' => ['final' ,"fine",'finest', 'finer', 'ugly', 'hard']],

            ];
            Variant::create([
                'value_json' => json_encode($array),
                'id_sentence' => 13,
            ]);
            $array = [
                ['0' => [ "You" , "Ours", 'Us', "Yours", "We", 'Our'] ],
                ['1' => ["doesn't" ,"didn't", "not", "don't", 'we', "knowing"]],
                ['2' => ['know' ,"knows",'knew', 'knowing', 'knowed', 'know']],
                ['3' => ['your' ,"don't",'knows', 'you', 'yours', 'us']],

            ];
            Variant::create([
                'value_json' => json_encode($array),
                'id_sentence' => 14,
            ]);
            $array = [
                ['0' => [ "Him" , "Sayes", 'He', "It", "His", 'Its'] ],
                ['1' => ["doesn't" ,"didn't", "not", "don't", 'we', "says"]],
                ['2' => ['saying' ,"sayest",'says', 'said', 'sayed', 'say']],
                ['3' => ['anytime' ,"don't",'something', 'anything', 'his', 'say']],

            ];
            Variant::create([
                'value_json' => json_encode($array),
                'id_sentence' => 15,
            ]);
            $array = [
                ['0' => [ "When" , "Where", 'Do', "What", "Why", 'Going'] ],
                ['1' => ["doesn't" ,"didn't", "does", "do", 'rain', "doing"]],
                ['2' => ['it' ,"his",'its', 'it', 'rain', 'rainy']],
                ['3' => ['rainer' ,"rain",'rains', 'rainy', 'rainded', 'raining']],

            ];
            Variant::create([
                'value_json' => json_encode($array),
                'id_sentence' => 16,
            ]);




    }
}
