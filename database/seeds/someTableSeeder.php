<?php

use Illuminate\Database\Seeder;
use Myapp\SomeTable;

class someTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SomeTable::create([
            'test' => "HELLO",
        ]);

    }
}
