<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkIdUsersUsersTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usersTasks', function (Blueprint $table) {
            //
            $table->integer('id_users')->unsigned();
            $table->foreign('id_users')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usersTasks', function (Blueprint $table) {
            //
            $table->dropForeign(['id_users']);
            $table->dropColumn('id_users');
        });
    }
}
