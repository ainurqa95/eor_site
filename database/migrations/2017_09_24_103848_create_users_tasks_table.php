<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersTasks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('date');
            $table->string('answer_json');
            $table->integer('id_task')->unsigned();
            $table->foreign('id_task')->references('id')->on('tasks');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersTasks');
    }
}
