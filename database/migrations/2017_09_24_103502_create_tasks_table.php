<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_sentence')->unsigned();
            $table->foreign('id_sentence')->references('id')->on('sentences');
            $table->integer('id_theme')->unsigned();
            $table->foreign('id_theme')->references('id')->on('themes');
            $table->integer('id_var')->unsigned();
            $table->foreign('id_var')->references('id')->on('variants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
