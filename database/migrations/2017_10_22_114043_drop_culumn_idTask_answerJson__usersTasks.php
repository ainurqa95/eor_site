<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropCulumnIdTaskAnswerJsonUsersTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usersTasks', function (Blueprint $table) {

            if (Schema::hasColumn('usersTasks','id_task')) {
                $table->dropForeign(['id_task']);
                $table->dropColumn('id_task');
            }
            if (Schema::hasColumn('usersTasks','date')) {
                $table->dropColumn('date');
            }
            if (!Schema::hasColumn('usersTasks','created_at')) {
                $table->timestamps();
            }
            if (Schema::hasColumn('usersTasks','answer_json')){
                $table->dropColumn('answer_json');
            }


            if (!Schema::hasColumn('usersTasks','id_theme')) {
                $table->integer('id_theme')->unsigned();
                $table->foreign('id_theme')->references('id')->on('themes');
            }else{
                $table->foreign('id_theme')->references('id')->on('themes');
            }

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usersTasks', function (Blueprint $table) {
            //
            if (!Schema::hasColumn('usersTasks','id_task')) {
                $table->integer('id_task')->unsigned();
                $table->foreign('id_task')->references('id')->on('tasks');
            }
            if (!Schema::hasColumn('usersTasks','answer_json'))
                $table->string('answer_json');

            if (Schema::hasColumn('usersTasks','created_at')) {
                $table->dropForeign(['id_theme']);
                $table->dropColumn('id_theme');
            }
            if (Schema::hasColumn('usersTasks','created_at'))
                $table->dropColumn('created_at');
            if (Schema::hasColumn('usersTasks','updated_at'))
                $table->dropColumn('updated_at');
        });
    }
}
