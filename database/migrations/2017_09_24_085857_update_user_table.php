<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user'))
            Schema::table('user', function (Blueprint $table) {
                //
                if (!Schema::hasColumn('user','birthday'))
                     $table->date('birthday');

                if (Schema::hasColumn('user','email'))
                     $table->string('email',250)->change();
   

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            //
            if (Schema::hasColumn('user','birthday'))
                    $table->dropColumn('birthday');
       
                if (Schema::hasColumn('user','email'))
                     $table->string('email',500)->change();
   
        });
    }
}
