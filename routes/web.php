<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/about/{id?}', [ 'uses' => 'SiteController@about', 'as' => 'about', '']);

    Route::get('/posts',  ['uses' =>'SiteController@getPosts' , 'as' => 'posts']);

    Route::get('/post/{id?}',  ['uses' =>'SiteController@getOnePost', 'as' => 'post']);







// REST 
	Route::get('/pages/add', [ 'uses' => 'Rest\SiteControllerResource@add', 'as' => 'add']);
	Route::resource('/pages', 'Rest\SiteControllerResource',['except' => ['destroy'] ]); // содержит круд методы





//








    Route::get('/', [ 'uses' => 'SiteController@index', 'as' => 'home']);

    Route::get('/courses', [ 'uses' => 'SiteController@courses', 'as' => 'courses']);


    Route::get('/start/courses/{id}', [ 'uses' => 'SiteController@startCourse', 'as' => 'start-course','middleware' =>'auth']);

    Route::get('/courses/{id}', [ 'uses' => 'SiteController@taskGo', 'as' => 'task-go']);

    Route::post('/taskNext', [ 'uses' => 'SiteController@taskNext', 'as' => 'task-next']);




    Route::match(['get','post'], '/sign-in', [ 'uses' => 'UserController@signIn', 'as' => 'sign-in', 'middleware' => 'user']);

    Route::match(['get','post'], '/sign-up', [ 'uses' => 'UserController@signUp', 'as' => 'sign-up']);


    Route::match(['get','post'],'/addScore', [ 'uses' => 'SiteController@addScore']);

    Route::group( ['prefix' => 'admin', 'middleware' => 'auth'] ,function(){ // для адин панели например


        Route::get('/', [ 'uses' => 'AdminController@index', 'as' => 'admin-index']);

        Route::get('/themes', [ 'uses' => 'AdminController@showThemes', 'as' => 'admin-themes']);

        Route::post('/themes/add', [ 'uses' => 'AdminController@addTheme', 'as' => 'admin-add-theme']);

    });




Route::get('/training', [ 'uses' => 'TrainingController@index', 'as' => 'training']);







// 		TRAINING

// Route::get('/', ['as' => 'home', function () {
//     return view('welcome');
// }]);

Route::get('/article/{id}', ['as' => 'article', function ($id) {
    echo "string = ". $id;

}]);

Route::get('/page/{cat}/{id}', function ($cat="nul",$id = 0) { // вопрос значит не обязателен
	// print_r($_ENV);
	echo "id = ". $id;
    // return view('form');
})->where( ['id'=> '[0-9]+','cat' =>'[a-z]+']);

Route::post('/page', function () {
	print_r($_ENV);
    // return view('site/page');
});


Route::match(['get','post'],'/send', function () { // сразу и get и пост
	print_r($_POST);
    // return view('site/page');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

